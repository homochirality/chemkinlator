/*
 * Copyright 2017-2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QHeaderView>
#include <QKeyEvent>
#include <QLabel>
#include <QMessageBox>
#include <QStandardItem>
#include <QVBoxLayout>

#include <iostream>

#include "ui/newreactionswizard.h"

using std::make_unique;
using std::experimental::optional;
using std::experimental::nullopt;

namespace Ui {

namespace {
class MyQTableView : public QTableView {
  public:
    explicit MyQTableView(QWidget *parent = Q_NULLPTR) : QTableView(parent) {}
  protected:
    void keyPressEvent(QKeyEvent *event) override {
    /*
     *  std::ios::fmtflags f( std::cout.flags() );
     *  std::cout << "key pressed is: " << std::hex << std::showbase
     *            << event->key() << " from MyQTableView" << std::endl;
     *  std::cout.flags( f );
     */

      int key = event->key();
      // If the entered key is any character then send signal of activated index
      if (   key==Qt::Key_Minus
         ||  key==Qt::Key_Less
         ||  key==Qt::Key_Space
         || (key>=Qt::Key_0 && key<=Qt::Key_9)
         || (key>=Qt::Key_A && key<=Qt::Key_Z)) {

        activated( currentIndex() );

      // If the key pressed is any other 'character', nothing should happen
      } else if (key>=Qt::Key_Exclam && key<=Qt::Key_ydiaeresis) {
        return;
      }
      QTableView::keyPressEvent(event);
    }
};
} // namespace

NewReactionsWizard::NewReactionsWizard(QWidget *parent, const QStringList &reacts)
  : QWizard(parent),
    askForReactionsPage( make_unique<QWizardPage>() ),
    askForReactionsLayout( make_unique<QVBoxLayout>() ),
    tableView( make_unique<MyQTableView>(this) ),
    modelOnlyRows( make_unique<QStandardItemModel>(0, 1, tableView.get()) ),
    possible_reactions(reacts)
{
  cancelButtonPressedBool = false;

  this->setWindowTitle("New reactions simulation");

  // General settings of Wizard function
  askForReactionsPage->setTitle("Enter reactions");
  askForReactionsPage->setLayout(askForReactionsLayout.get());

  // creating label to add
  labelExplanation = make_unique<QLabel>(
      "The format to write a single reaction is either:\n"
      "'CH4 + 2O2 -> CO2 + 2H2O', or\n"
      "'2NaCl + CaCO3 <-> Na2CO3 + CaCl2' if you want to save space writing a reversible reaction\n"
      "If you want to, you can write a reaction without one of its members (left or right), i.e.,\n"
      "'-> x', or 'y ->'");
  labelExplanation->setWordWrap(true);

  // creating table to hold reactions, and defining its behaivor
  tableView->setModel( modelOnlyRows.get() );
  tableView->horizontalHeader()->hide();
  tableView->setColumnWidth(0, 350);

  {
    int i;
    for(i = 0; i < possible_reactions.size(); i++) {
      modelOnlyRows->insertRow(i, new QStandardItem{possible_reactions[i]});
    }
    modelOnlyRows->insertRow(i, new QStandardItem{ "" });
  }

  // adding all elements to the wizard and page
  askForReactionsLayout->addWidget(labelExplanation.get());
  askForReactionsLayout->addWidget(tableView.get());
  this->addPage(askForReactionsPage.get());

  connect(modelOnlyRows.get(), &QStandardItemModel::itemChanged, this, &NewReactionsWizard::verifyModifiedItems);
  connect(tableView.get(), &QTableView::activated, this, &NewReactionsWizard::addRowIfLastRowActivated);
 /*
  * connect(modelOnlyRows.get(), &QStandardItemModel::rowsInserted,
  *     [this](const QModelIndex &, int, int) mutable { });
  */
 /*
  * connect(tableView->selectionModel(), &QItemSelectionModel::currentChanged,
  *     [this](const QModelIndex &index, const QModelIndex &previous) mutable {
  *       std::cout << "hey, hey! row: " << index.row() << std::endl;
  *     });
  */
}

//NewReactionsWizard::~NewReactionsWizard() {
//  std::cout << "Destroying NewReactionsWizard object" << std::endl;
//}

// detect if cancel button from Wizard window was pressed
void NewReactionsWizard::rejected() {
  this->cancelButtonPressedBool = true;
  QWizard::rejected();
}

void NewReactionsWizard::verifyModifiedItems(QStandardItem *itemChanged) {
  int rowCount = this->modelOnlyRows->rowCount();
  int itemChangedRow = this->modelOnlyRows->indexFromItem(itemChanged).row();

  //std::cout << "Row count: " << rowCount << " Row modified: " << itemChangedRow << std::endl;

  if ( itemChangedRow == rowCount-1 ) {
    // if the item modified was the last item in the list, add a new empty item to the end
    if ( QRegExp("\\s*").exactMatch( this->modelOnlyRows->item(rowCount-1)->text() ) ) {
      rowCount--; // the last item was modified but is empty, no need to remove it
    } else {
      this->modelOnlyRows->insertRow(rowCount, new QStandardItem{ "" });
    }
  } else {
    rowCount--;
  }

  deleteEmptyRows(rowCount);
  //std::cout << "Well, some item has been changed!" << std::endl;
}

void NewReactionsWizard::deleteEmptyRows(int rowCount) {
  // Removing empty rows (should be at most one row)
  for (int i = 0, deleted = 0; i < rowCount - deleted; ) {
    QStandardItem *item = this->modelOnlyRows->item(i);
    if ( QRegExp("\\s*").exactMatch( item->text() ) ) {
      // Setting index to next item, this prevents tableView to lose focus from the application
      QModelIndex index = this->modelOnlyRows->index(i+1, 0);
      this->tableView->setCurrentIndex(index);

      // Deleting row (it has only an item)
      auto items = this->modelOnlyRows->takeRow(i);
      for (auto&& takenItem : items) { delete takenItem; }
      deleted++;
    } else {
      i++;
    }
  }
}

bool NewReactionsWizard::cancelButtonPressed() {
  return cancelButtonPressedBool;
}

optional<QStringList> NewReactionsWizard::getReactions() {
  // getting reactions from Wizard window
  QStringList listOfReactions;

  // if cancelButton was pressed, then return an empty list
  if(this->cancelButtonPressed()) { return nullopt; }

  int totalRows = modelOnlyRows->rowCount();
  for(int i=0; i < totalRows - 1; i++) {
    QString reaction = modelOnlyRows->item(i, 0)->text();
    if (! reaction.isEmpty()) {
      listOfReactions << reaction;
    }
  }

  // test if all strings are reactions
  if (listOfReactions.length() == 0) { return nullopt; }

  //std::cout << "Hey wizard!" << std::endl;
  return listOfReactions;
}

int NewReactionsWizard::exec() {

  bool show_menu = true;
  int res;
  while(show_menu) {
    res = QWizard::exec();

    auto listOfReactions = getReactions();

    if(!listOfReactions.has_value() || cancelButtonPressed()) {
      show_menu = false;
    } else {
      // Checking whether all strings can be interpreted as reactions
      QString qar = QString(R"(\d*\s*[a-zA-Z][a-zA-Z0-9-_]*)");  // species name and coefficient
      QString oneSide = R"(\s*()" + qar + R"((\s*\+\s*)" + qar + R"(\s*)*\s*)?)";
      auto react_regexp = QRegExp(oneSide + "<?->" + oneSide);
      auto reactions = listOfReactions.value();
      int n_reacts = reactions.length();
      int i;

      for (i = 0; i < n_reacts; i++) {
        if (! react_regexp.exactMatch(reactions[i])) {
          break;
        }
      }

      if(i<n_reacts) { // string `i` cannot be interpreted as a reaction
        auto reply = QMessageBox::question(
            static_cast<QWidget*>(parent()),
            "Error processing a reaction",
            QString(
              "The string \"%1\" cannot be interpreted as a reaction.\n"
              "Do you wish to modify the string?\n"
              "If you press 'No', the reaction network will not be loaded."
            ).arg(reactions[i]),
            QMessageBox::Yes | QMessageBox::No
        );
        if(reply == QMessageBox::No) {
          show_menu = false;
          cancelButtonPressedBool = true;
        }
      } else {
        show_menu = false;
      }
    }

  }

  return res;
}

void NewReactionsWizard::keyPressEvent(QKeyEvent *event) {
  /*
   *std::ios::fmtflags f( std::cout.flags() );
   *std::cout << "key pressed is: " << std::hex << std::showbase
   *          << event->key() << " from Wizard" << std::endl;
   *std::cout.flags( f );
   */

  // Detecting if pressed key is enter/return
  if (event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return) {
    if (event->modifiers() == Qt::ShiftModifier || event->modifiers() == Qt::ControlModifier) {
      // The combination Ctrl+Enter or Shift+Enter are shortcurts to press "Finish" in Wizard
      this->accept();
    } else {
      //tableView->keyPressEvent(event);
      tableView->edit(tableView->currentIndex());
    }
  } else {
    QWizard::keyPressEvent(event);
  }
}

void NewReactionsWizard::addRowIfLastRowActivated(const QModelIndex index) {
  int rowCount = this->modelOnlyRows->rowCount();
  if (index.row() == rowCount - 1) {
    this->modelOnlyRows->insertRow(rowCount, new QStandardItem{ "" });
    deleteEmptyRows(rowCount-1);
  }
}

} // namespace Ui
