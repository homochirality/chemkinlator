/*
 * Copyright 2017-2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_UI_NEWREACTIONSWIZARD_H_
#define SRC_UI_NEWREACTIONSWIZARD_H_

#include <QStandardItemModel>
#include <QStringList>
#include <QTableView>
#include <QWizard>
#include <memory>

#include "optional.hpp"

namespace Ui {

/*!
 * @brief Wizard asking user for input of new reaction network
 */
class NewReactionsWizard : public QWizard {
  bool cancelButtonPressedBool;
  std::unique_ptr<QWizardPage>        askForReactionsPage;
  std::unique_ptr<QVBoxLayout>        askForReactionsLayout;
  std::unique_ptr<QTableView>         tableView;
  std::unique_ptr<QStandardItemModel> modelOnlyRows;
  std::unique_ptr<QLabel>             labelExplanation;
  QStringList                         possible_reactions;
  /*!
   * @brief Returns `true` if the user pressed the cancel button.
   *
   * Notice that `modelOnlyRows` could contain some lines, they're not erased
   * by pressing cancel, therefore we should ignore anything written by the
   * user in there if the user presses cancel
   */
  bool cancelButtonPressed();

private slots:
  void verifyModifiedItems(QStandardItem *itemChanged);
  void addRowIfLastRowActivated(const QModelIndex index);
  void deleteEmptyRows(int rowCount);

signals:
  void rejected();

public:
  explicit NewReactionsWizard(QWidget *parent = 0, const QStringList &reacts = {});
  //~NewReactionsWizard();
  /*!
   * @brief Processes all reactions entered by the user and returns all of them
   *        only if they are all correct reactions, otherwise returns `nullopt`
   */
  std::experimental::optional<QStringList> getReactions();
  int exec() override;

protected:
  void keyPressEvent(QKeyEvent *event) override;
};
} // namespace Ui

#endif // SRC_UI_NEWREACTIONSWIZARD_H_
