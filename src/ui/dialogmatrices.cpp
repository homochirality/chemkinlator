/*
 * Copyright 2017-2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QDebug>
#include <QFileDialog>
#include <QFont>
#include <QFontDatabase>
#include <QMenuBar>
#include <QPushButton>
#include <QToolBox>
#include <QVBoxLayout>
#include <qwt_text_label.h>

#include "ui/dialogmatrices.h"

namespace Ui {

QDialogMatrices::QDialogMatrices(QWidget *parent)
  : QDialog(parent),
    itemsWidget(new QToolBox(this))
{
  // ensambling dialog
  auto matricesLayout = new QVBoxLayout(this);
  matricesLayout->addWidget(itemsWidget);
  setLayout(matricesLayout);
  resize(400, 450);

  auto savePlainText = new QPushButton(this);
  savePlainText->setText("Save in plain text");
  matricesLayout->addWidget(savePlainText);

  connect(savePlainText, &QPushButton::clicked, this, &QDialogMatrices::savePlainText);
}

void QDialogMatrices::addItem(
    const QString &title,
    const QStringList & eqs,
    bool qwtmath
) {
  auto diffeqsLayout = new QVBoxLayout();
  auto label = new QWidget(this);
  label->setLayout(diffeqsLayout);

  for (auto&& eq : eqs) {
    QWidget *eqLabel;
    if (qwtmath) {
      eqLabel = new QwtTextLabel(QwtText(eq, QwtText::MathMLText));
    } else {
      auto qLabel = new QLabel(eq, this);
      qLabel->setWordWrap(true);
      qLabel->setFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));
      qLabel->setContentsMargins(5, 5, 5, 5);
      eqLabel = qLabel;
    }
    diffeqsLayout->addWidget(eqLabel, 0, Qt::AlignLeft);
  }
  diffeqsLayout->addStretch();

  itemsWidget->addItem(label, title);

  // Saving equations as plain text to save latter
  if (qwtmath) {
    plainTextItems << "====== " << title << " (in MathML) ======" << "\n";
    for(auto&& eq: eqs) {
      plainTextItems << eq.simplified().remove(' ') << "\n\n";
    }
  } else {
    plainTextItems
      << "====== " << title << " ======" << "\n"
      << eqs.join("\n") << "\n";
  }
}

void QDialogMatrices::addItem(
    const QString &title,
    const QString &eq,
    bool qwtmath
) {
  QStringList eqs;
  eqs << eq;
  addItem(title, eqs, qwtmath);
}

QString QDialogMatrices::itemsToPlainText() const {
  return plainTextItems.join("");
}

void QDialogMatrices::savePlainText() {
  //qDebug().nospace() << qUtf8Printable(itemsToPlainText());

  QFileDialog dialog(this);
  dialog.setAcceptMode(QFileDialog::AcceptSave);
  dialog.setFileMode(QFileDialog::AnyFile);
  dialog.setDirectory(QDir::homePath());
  dialog.setNameFilter(tr("Plain Text (*.txt)"));
  dialog.setDefaultSuffix("txt");
  dialog.exec();

  if( dialog.selectedFiles().isEmpty() ) { return; }

  QString fileName = dialog.selectedFiles().first();

  if (QFile::exists(fileName)) { QFile::remove(fileName); }

  QFile file(fileName);

  if (file.open(QIODevice::WriteOnly)) {
    QTextStream out(&file);
    out << itemsToPlainText();
    file.close();
  }
}

} // namespace Ui
