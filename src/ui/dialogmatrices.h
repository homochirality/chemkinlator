/*
 * Copyright 2017-2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QDialog>
#include <QLabel>
#include <QWidget>

#ifndef SRC_UI_DIALOGMATRICES_H_
#define SRC_UI_DIALOGMATRICES_H_

class QToolBox;

namespace Ui {
class MainWindow;

/*!
 * @brief Dialog that shows the input reactions and the Stoichiometric, and Reactions
 *        Order Matrices
 */
class QDialogMatrices : public QDialog {
  QToolBox *itemsWidget;
  QStringList plainTextItems;

public:
  explicit QDialogMatrices(QWidget *parent = 0);
  void addItem(const QString &title, const QStringList &, bool qwtmath = false);
  void addItem(const QString &title, const QString &, bool qwtmath = false);
  QString itemsToPlainText() const;

public slots:
  void savePlainText();
};
} // namespace Ui

#endif // SRC_UI_DIALOGMATRICES_H_
