/*
 * Copyright 2017-2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_SIMULATORS_SIMULATOR_H_
#define SRC_SIMULATORS_SIMULATOR_H_

#include <memory>

#include "../reactiondetails.h"
#include "../simulationdetails.h"
#include "../simulationresults.h"

enum class SimulatorDebug : bool { On, Off };

class Simulator {
 protected:
  const ReactionDetails &reactionDetails;
  const SimulationDetails<double> &simuDetails;

 public:
    explicit Simulator(const ReactionDetails &rd, const SimulationDetails<double> &sd) :
      reactionDetails(rd), simuDetails(sd) {}

    virtual std::unique_ptr<SimulationResults> run(SimulatorDebug = SimulatorDebug::On) = 0;

 protected:
    std::unique_ptr<SimulationResults>
      convertConcentrationsFromFortranToMap(
        const ReactionDetails &reactionDetails,
        double *concentrations_through_time,
        int numSpecies,
        int num_time_intervals,
        int num_time_intervals_from_fortran,
        std::vector<std::string> additionalCols);
};

#endif  // SRC_SIMULATORS_SIMULATOR_H_
