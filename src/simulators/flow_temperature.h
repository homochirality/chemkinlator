/*
 * Copyright 2017-2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_SIMULATORS_FLOW_TEMPERATURE_H_
#define SRC_SIMULATORS_FLOW_TEMPERATURE_H_

#include "simulator.h"

class FlowTemperature: public Simulator {
  QMap<QString, double> flowEntrySpecies;
  QMap<QString, double> concentrationEntrySpecies;
  QMap<QString, double> activationEnergy;
  QMap<QString, double> preExpFactor;
  QMap<QString, double> reactionsEnthalpy;
  double initialTemp;
  double temp_ambient;
  double temp_reactives;
  double volume_reactor;
  double kappa;
  double heat_capacity;

public:
  explicit FlowTemperature(const ReactionDetails &rd, const SimulationDetails<double> &sd);

  std::unique_ptr<SimulationResults> run(SimulatorDebug = SimulatorDebug::On);

  void setFlowEntrySpecies(const QString &, double);
  void setConcentrationEntrySpecies(const QString &, double);
  void setActivationEnergy(const QString &, double);
  void setPreExpFactor(const QString &, double);
  //! Set reaction enthalpy
  void setReactionEnthalpy(const QString &, double);
  void setInitialTemp(double);
  void setTemp_ambient(double);
  void setTemp_reactives(double);
  void setVolume_reactor(double);
  void setKappa(double);
  void setHeatCapacity(double);
};

#endif  // SRC_SIMULATORS_FLOW_TEMPERATURE_H_
