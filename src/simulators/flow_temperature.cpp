/*
 * Copyright 2017-2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QDebug>
#include <QListIterator>
#include <QStringList>

#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#include "flow_temperature.h"

///> The purpose of this buffer is to create a slightly longer array where the fortran code
///  will write all its output. It is very difficult to calculate precisely how many executions
///  (how much data) will fortran produce, because of rounding errors.
#define BUFFER_TIME_INTERVALS 10

using std::unordered_map;
using std::string;
using std::vector;
using MapStringVectorDouble = unordered_map<string, vector<double>>;

// anything declared inside an anonymous namespace is only visible inside this file
namespace {
/*
 *  module chemical_simulator_with_temp
 *     subroutine run_simulation(n_species_in, n_reactions_in, &
 *                               activation_energy_in, preexponential_factor_in, concentrations, &
 *                               stoichiometric_matrix_in, reactions_order_matrix_in, &
 *                               flow_entry_for_species_in, concentration_entry_for_species_in, &
 *                               Temp_ambient_in, Temp_reactives_in, Volume_reactor_in, kappa_in, &
 *                               reactions_enthalpy_in, heat_capacity_in, &
 *                               T_0, Tmax, &
 *                               Delta, ATOL, RTOL, &
 *                               num_time_intervals, concentrations_through_time, &
 *                               output_to_stdout, failure_flag)
 *
 *      integer, intent(in) :: n_species_in, n_reactions_in
 *      double precision, dimension(n_species_in+1), intent(inout) :: concentrations
 *      double precision, intent(in) :: &
 *        T_0, Tmax, Delta, RTOL, Temp_ambient_in, Temp_reactives_in, Volume_reactor_in, kappa_in, &
 *        heat_capacity_in
 *      double precision, dimension(n_species_in), intent(in), target :: &
 *        flow_entry_for_species_in, concentration_entry_for_species_in
 *      double precision, dimension(n_reactions_in), intent(in), target :: &
 *        activation_energy_in, preexponential_factor_in, reactions_enthalpy_in
 *      double precision, dimension(1), intent(in) :: ATOL
 *      integer, intent(inout) :: num_time_intervals
 *      double precision, dimension(num_time_intervals, n_species_in+2), intent(inout) :: &
 *        concentrations_through_time
 *      integer, dimension(n_species_in, n_reactions_in), intent(in), target :: &
 *        stoichiometric_matrix_in, reactions_order_matrix_in
 *      logical(kind=1), intent(in) :: output_to_stdout
 *      logical(kind=1), intent(out) :: failure_flag
 */
extern "C" {
void __chemical_simulator_with_temp_MOD_run_simulation(
    int *n_species, int *n_reactions,
    double activation_energy_in[], double preexponential_factor_in[],
    double concentrations[], int stoichiometric_matrix_in[],
    int reactions_order_matrix_in[],
    double flow_entry_for_species_in[], double concentration_entry_for_species_in[],
    double *Temp_ambient_in, double *Temp_reactives_in,
    double *Volume_reactor_in, double *kappa_in,
    double reactions_enthalpy_in[], double *heat_capacity_in,
    double *T_0, double *Tmax, double *Delta,
    double *ATOL, double *RTOL, int *num_time_intervals,
    double concentrations_through_time[], bool *output_to_stdout,
    bool *failure_flag);
}
/***************** END OF EXTERNAL DEFINITIONS *****************/
} // anonymous namespace

FlowTemperature::FlowTemperature(const ReactionDetails &rd, const SimulationDetails<double> &sd) :
  Simulator(rd, sd) {}

std::unique_ptr<SimulationResults>
FlowTemperature::run(SimulatorDebug debug) {

  double T_0   = 0;
  double Tmax  = simuDetails.getTmax();
  double Delta = simuDetails.getDelta();
  double ATOL  = simuDetails.getATOL();
  double RTOL  = simuDetails.getRTOL();

  int numSpecies   = reactionDetails.numSpecies();
  int numReactions = reactionDetails.numReactions();
  int totalColumns = numSpecies + 1;
  auto *concentrations         = new double[totalColumns];
  auto *stoichiometric_matrix  = new int[numSpecies * numReactions];
  auto *reactions_order_matrix = new int[numSpecies * numReactions];
  vector<string> additionalCols;

  const int num_time_intervals = (Tmax - T_0) / Delta + 1 + BUFFER_TIME_INTERVALS;
  int num_time_intervals_from_fortran = num_time_intervals;
  auto concentrations_through_time =
      new double[num_time_intervals * (totalColumns + 1)];
  bool output_to_stdout, failure_flag;

  // copying initial concentrations to a format readable from fortran
  const QMap<QString, double> &initialConcentrations =
      reactionDetails.getInitialConcentrations();
  QListIterator<QString> species(reactionDetails.getSpecies());
  for (int i = 0; species.hasNext(); i++) {
    concentrations[i] = initialConcentrations.value(species.next());
  }

  // copying matrices to a format readable from fortran
  const QMap<QPair<QString, QString>, int> &stoichiometricMatrix =
      reactionDetails.getStoichiometricMatrix();
  const QMap<QPair<QString, QString>, int> &reactionsOrderMatrix =
      reactionDetails.getReactionsOrderMatrix();

  QListIterator<QString> reactions(reactionDetails.getReactions());
  for (int i = 0; reactions.hasNext(); i++) {
    species.toFront();
    QString reaction = reactions.next();
    for (int j = 0; species.hasNext(); j++) {
      QString aSpecies = species.next();
      stoichiometric_matrix[i * numSpecies + j] =
          stoichiometricMatrix.value(qMakePair(reaction, aSpecies), 0);
      reactions_order_matrix[i * numSpecies + j] =
          reactionsOrderMatrix.value(qMakePair(reaction, aSpecies), 0);
    }
  }

  auto *flow_entry_for_species          = new double[numSpecies];
  auto *concentration_entry_for_species = new double[numSpecies];
  auto *activation_energy               = new double[numReactions];
  auto *preexponential_factor           = new double[numReactions];
  auto *reactions_enthalpy              = new double[numReactions];

  // copying reactions to a format readable from fortran
  // TODO(helq): Add assertion "numReactions == reactions.size()". Use
  // throw_assert from https://www.softwariness.com/articles/assertions-in-cpp/
  reactions.toFront();
  for (int i = 0; reactions.hasNext(); i++) {
    auto reaction = reactions.next();
    activation_energy[i] = activationEnergy.value(reaction);
    preexponential_factor[i] = preExpFactor.value(reaction);
    reactions_enthalpy[i] = reactionsEnthalpy.value(reaction);
  }

  species.toFront();
  for (int i = 0; species.hasNext(); i++) {
    QString aSpecies = species.next();
    flow_entry_for_species[i]          = flowEntrySpecies.value(aSpecies);
    concentration_entry_for_species[i] = concentrationEntrySpecies.value(aSpecies);
  }

  concentrations[numSpecies] = initialTemp;

  double temp_ambient      = this->temp_ambient,
         temp_reactives    = this->temp_reactives,
         volume_reactor    = this->volume_reactor,
         kappa             = this->kappa,
         heat_capacity     = this->heat_capacity;

  if (debug == SimulatorDebug::On) {
    qDebug("\nConcentrations entered:");
    species.toFront();
    for (int i = 0; species.hasNext(); i++) {
      QString aSpecies = species.next();
      double concentration = initialConcentrations.value(aSpecies);
      qDebug("%s",
             QString("%1: %2")
                 .arg(aSpecies)
                 .arg(concentration, 0, 'g', 3)
                 .toLatin1().data());
    }

    qDebug("\nVariables for simulation entered:");
    qDebug() << "T_0  " << T_0;
    qDebug() << "Tmax " << Tmax;
    qDebug() << "Delta" << Delta;
    qDebug() << "ATOL " << ATOL;
    qDebug() << "RTOL " << RTOL;
    qDebug("\n");
  }

  if(num_time_intervals - num_time_intervals_from_fortran == BUFFER_TIME_INTERVALS) {
    std::cerr << "WARNING: " << (num_time_intervals-BUFFER_TIME_INTERVALS) << " should be " << num_time_intervals_from_fortran << " !!!" << std::endl;
  }

  output_to_stdout = debug == SimulatorDebug::On;

  __chemical_simulator_with_temp_MOD_run_simulation(
      &numSpecies, &numReactions,
      activation_energy, preexponential_factor, concentrations,
      stoichiometric_matrix, reactions_order_matrix,
      flow_entry_for_species, concentration_entry_for_species,
      &temp_ambient, &temp_reactives,
      &volume_reactor, &kappa,
      reactions_enthalpy, &heat_capacity,
      &T_0, &Tmax, &Delta,
      &ATOL, &RTOL, &num_time_intervals_from_fortran,
      concentrations_through_time, &output_to_stdout, &failure_flag);

  additionalCols.push_back("Temperature");

  auto toret = convertConcentrationsFromFortranToMap(
      reactionDetails, concentrations_through_time, numSpecies,
      num_time_intervals, num_time_intervals_from_fortran,
      additionalCols);

  delete[] reactions_enthalpy;
  delete[] preexponential_factor;
  delete[] activation_energy;
  delete[] concentration_entry_for_species;
  delete[] flow_entry_for_species;
  delete[] concentrations_through_time;
  delete[] reactions_order_matrix;
  delete[] stoichiometric_matrix;
  delete[] concentrations;

  return toret;
}

void
FlowTemperature::setFlowEntrySpecies(const QString &aSpecies, double flow) {
  // TODO: Check for the QString to be in the list of reactions
  flowEntrySpecies.insert(aSpecies, flow);
}

void
FlowTemperature::setConcentrationEntrySpecies(const QString &aSpecies, double concentr) {
  // TODO: Check for the QString to be in the list of reactions
  concentrationEntrySpecies.insert(aSpecies, concentr);
}

void
FlowTemperature::setActivationEnergy(const QString &reaction, double energ) {
  // TODO: Check for the QString to be in the list of reactions
  activationEnergy.insert(reaction, energ);
}

void
FlowTemperature::setPreExpFactor(const QString &reaction, double factor) {
  // TODO: Check for the QString to be in the list of reactions
  preExpFactor.insert(reaction, factor);
}

void
FlowTemperature::setReactionEnthalpy(const QString &reaction, double enthalpy) {
  // TODO: Check for the QString to be in the list of reactions
  reactionsEnthalpy.insert(reaction, enthalpy);
}

void
FlowTemperature::setInitialTemp(double temp) {
  initialTemp = temp;
}

void
FlowTemperature::setTemp_ambient(double temp) {
  temp_ambient = temp;
}

void
FlowTemperature::setTemp_reactives(double temp) {
  temp_reactives = temp;
}

void
FlowTemperature::setVolume_reactor(double vol) {
  volume_reactor = vol;
}

void
FlowTemperature::setKappa(double k) {
  kappa = k;
}

void
FlowTemperature::setHeatCapacity(double c) {
  heat_capacity = c;
}
