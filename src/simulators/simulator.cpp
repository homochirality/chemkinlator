/*
 * Copyright 2017-2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "simulator.h"

using std::unordered_map;
using std::string;
using std::vector;
using MapStringVectorDouble = unordered_map<string, vector<double>>;

std::unique_ptr<SimulationResults>
Simulator::convertConcentrationsFromFortranToMap(
              const ReactionDetails &reactionDetails,
              double *concentrations_through_time,
              int numSpecies,
              int num_time_intervals,
              int num_time_intervals_from_fortran,
              vector<string> additionalCols) {

  auto contsThruTime = std::make_shared<MapStringVectorDouble>();

  const QStringList &species = reactionDetails.getSpecies();

  for (int j = 0; j <= numSpecies + additionalCols.size(); j++) {
    string columnName;

    if(j == 0) {
      columnName = "Time";
    } else if(j<=numSpecies) {
      columnName = species.at(j - 1).toStdString();
    } else {
      columnName = additionalCols[j-numSpecies-1];
    }

    vector<double> columnList;
    columnList.reserve(num_time_intervals_from_fortran);

    for (int i = 0; i < num_time_intervals_from_fortran; i++) {
      columnList.push_back(
          concentrations_through_time[j * num_time_intervals + i]);
    }

    //std::cout << columnName << " ";
    //std::copy(columnList.begin(), columnList.end(), std::ostream_iterator<double>(std::cout, " "));
    //std::cout << std::endl;

    contsThruTime->insert( {columnName, std::move(columnList)} );

    //std::cout << columnName << " ";
    //std::copy(contsThruTime->at(columnName).begin(), contsThruTime->at(columnName).end(), std::ostream_iterator<double>(std::cout, " "));
    //std::cout << std::endl << std::endl;

  }

  //std::cout << contsThruTime->at("Time")[0] << std::endl;
  return std::make_unique<SimulationResults>( contsThruTime );
  // qDebug() << "array from fortran to QMapList copied";
}
