/*
 * Copyright 2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "factorytab.h"
#include "tabs/bifurcationtab.h"
#include "tabs/flowtemperaturetab.h"
#include "tabs/timeseriestab.h"

using std::experimental::make_optional;
using std::experimental::nullopt;
using std::experimental::optional;
using std::make_shared;
using std::shared_ptr;
using std::unique_ptr;
using std::pair;

namespace FactoryTab {

pair<QString, shared_ptr<MainWindowTab>>
defaultTab(unique_ptr<MainWindowState> mwstate, QWidget *widget) {
  return {
    "Time Series",
    make_shared<TimeSeriesTab>(std::move(mwstate), widget)
  };
}

optional<shared_ptr<MainWindowTab>>
getTabPtr(
    QString tabtype,
    unique_ptr<MainWindowState> mwstate,
    QWidget *widget,
    const optional<QJsonObject> &tabInfo
) {
  shared_ptr<MainWindowTab> toret;

  if (tabtype == "standard" || tabtype == "timeseries") {
    toret = make_shared<TimeSeriesTab>(std::move(mwstate), widget);

  } else if (tabtype == "bifurcation") {
    toret = make_shared<BifurcationTab>(std::move(mwstate), widget, tabInfo);

  } else if (tabtype == "temperature") {
    toret = make_shared<FlowTemperatureTab>(std::move(mwstate), widget, tabInfo);

  } else {
    return nullopt;
  }

  return make_optional(toret);
}

QMap<QString, QString>
tabTypes() {
  return {
    {"timeseries",  "Time Series"},
    {"bifurcation", "Bifurcation"},
    {"temperature", "Flow/Temperature"},
  };
}

}; // namespace FactoryTab
