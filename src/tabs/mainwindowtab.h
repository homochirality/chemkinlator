/*
 * Copyright 2017-2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SRC_TABS_MAINWINDOWTAB_H_
#define SRC_TABS_MAINWINDOWTAB_H_

#include <QAction>
#include <QJsonObject>

#include <memory>
#include <vector>

#include "mainwindowstate.h"
#include "reactiondetails.h"

// TODO: add examples of how to add new tabs

/*!
 * @brief Superclass for each tab on the MainWindow. Defines common behaivor for all tabs
 */
class MainWindowTab : public QWidget {
public:
  //virtual void execute_nogui(const ReactionDetails &, const SimulationDetails<double> &, const QStringList &) const = 0;
  /*!
   * @brief Executes simulations and renders them as defined by the tab inside the tab
   *
   * This method is called everytime the "Run Simulation" (from MainWindow) button is pushed,
   * and therefore it should run the simulation(s) defined in the tab.
   *
   * @param rd  Details for the reaction network as defined by the user in the MainWindow
   * @param sm  Details for the simulation as defined by the user in the MainWindow
   */
  virtual void execute(const ReactionDetails &, const SimulationDetails<double> &) = 0;

  /*!
   * @brief Returns `true` if the tab is still executing, `false` otherwise
   *
   * @return `true` if the tab is still executing, `false` otherwise
   */
  virtual bool isStillRunning() = 0;

  /*!
   * @brief Changes the "enable" state for each reaction box depending on the state of the tab
   *
   * Sometimes a tab will overwrite the values setted for the reaction rates, when this happens
   * it is useful to "disable" the box so that the user knows that the value reaction rate is
   * ignored.
   */
  virtual void reapplyBoxesState() = 0;

  /*!
   * @brief Returns all QActions defined by the tab
   *
   * @return A vector with all QActions defined by the tab
   */
  const std::vector<std::shared_ptr<QAction>>& getActions() { return actions; }

  /*!
   * @brief Cleans tab's interface or plotting area inside the tab.
   *
   * The tab's interface can get sometimes very messy, or simply it can have data that
   * isn't related anymore to the parameters setted on the principal interface. When this
   * happens it is useful to clean the tab's interface.
   */
  virtual void cleanTab() = 0;

  /*!
   * @brief Creates a json object that contains all the info to reload the tab as it
   *        currently is
   *
   * This method is meant to be used when saving the model into a file (.simu.json). What
   * is saved in the file should be enough to load the tab interface.
   */
  virtual QJsonObject toJsonObject() = 0;

public slots:
  /*!
   * @brief Force replot
   */
  virtual void replot() = 0;

protected:
  /*!
   * @brief Hidden constructor in which the internal data for MainWindowTab is initialized
   *
   * @param mwstate_ The current state of the MainWindow, used to hide reaction rate boxes
   * @param parent   A MainWindow object reference/pointer
   */
  explicit MainWindowTab(std::unique_ptr<MainWindowState> mwstate_, QWidget* parent)
      : QWidget(parent), mwstate(std::move(mwstate_)) {}

  /*!
   * @brief The current state of the MainWindow, used to hide reaction rate boxes
   */
  std::unique_ptr<MainWindowState> mwstate;

  /*!
   * QActions that the tab defines to be executed in demand from the user, they are to be
   * placed in the Tab menu. An example of a QAction is "Save Data" which saves the data
   * produced by the tab when executed.
   */
  std::vector<std::shared_ptr<QAction>> actions;
};

#endif  // SRC_TABS_MAINWINDOWTAB_H_
