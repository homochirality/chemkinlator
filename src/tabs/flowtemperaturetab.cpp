/*
 * Copyright 2017-2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//#include <QDebug>

#include "tabs/flowtemperaturetab.h"

#include <QDebug>
#include <QFileDialog>
#include <QJsonArray>
#include <QMessageBox>
#include <QTextStream>

#include <qwt_legend.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_renderer.h>
#include <qwt_symbol.h>
#include <qwt_text_label.h>

#include <fstream>
#include <array>

#include "simulators/flow_temperature.h"

using std::experimental::optional;
using std::unique_ptr;
using std::vector;

FlowTemperatureTab::~FlowTemperatureTab() = default;

FlowTemperatureTab::FlowTemperatureTab(unique_ptr<MainWindowState> mwstate, QWidget *parent, const optional<QJsonObject> &tabInfo_)
    : MainWindowTab(std::move(mwstate), parent),
      saveDataAction(std::make_shared<QAction>(this)),
      exportPlotAction(std::make_shared<QAction>(this)),
      qwt_plot(std::make_unique<QwtPlot>()) {

  // Setting up UI
  ui_temptab.setupUi(this);
  //int qscroll_height = ui_temptab.scrollable_column_widget->size().width();
  //ui_temptab.scrollable_column_widget->resize(240, qscroll_height);

  ui_temptab.splitter->addWidget(qwt_plot.get());

  ui_temptab.form_layout->setLabelAlignment(Qt::AlignRight);
  ui_temptab.form_layout->setFormAlignment(Qt::AlignHCenter | Qt::AlignTop);

  // Setting Kappa label with Kappa symbol
  QwtText kappa_text("<math><mrow><mi>&#x003BA;</mi></mrow></math>", QwtText::MathMLText);
  auto kappa_label = new QwtTextLabel(kappa_text);

  // kappa_layout and kappa_container are meant just to center the text
  auto kappa_layout = new QVBoxLayout();
  auto kappa_container = new QWidget(this);
  kappa_container->setLayout(kappa_layout);
  kappa_layout->addWidget(kappa_label, 0, Qt::AlignHCenter | Qt::AlignVCenter);

  ui_temptab.form_layout->setWidget(4, QFormLayout::LabelRole, kappa_container);
  // Finishing kappa setting

  qwt_plot->setTitle("Species Concentrations and Temperature");
  qwt_plot->setCanvasBackground(Qt::white);
  qwt_plot->enableAxis(QwtPlot::yRight);
  //qwt_plot->setAxisScale(QwtPlot::yLeft, 0.0, 10.0);
  qwt_plot->insertLegend(new QwtLegend());
  qwt_plot->setAxisTitle(QwtPlot::yLeft, "Concentrations");
  qwt_plot->setAxisTitle(QwtPlot::xBottom, "Time");

  QwtPlotGrid *grid = new QwtPlotGrid();
  grid->setMajorPen(Qt::gray, 0, Qt::DotLine);
  grid->setMinorPen(Qt::darkGray, 0, Qt::DotLine);
  grid->attach(qwt_plot.get());
  // End of UI setting

  saveDataAction->setText("Save Data");
  exportPlotAction->setText("Export Plot");
  saveDataAction->setEnabled(false); // disabling buttons to save data and plot
  exportPlotAction->setEnabled(false);
  executed = false;

  actions.emplace_back(saveDataAction);
  actions.emplace_back(exportPlotAction);

  connect(saveDataAction.get(), SIGNAL(triggered(bool)), this, SLOT(when_save_data_is_pushed(bool)));
  connect(exportPlotAction.get(), &QAction::triggered,
      [this](bool){
        QwtPlotRenderer renderer;
        QSizeF filesize = {
          static_cast<qreal>(this->qwt_plot->width())/3.6,
          static_cast<qreal>(this->qwt_plot->height())/3.6
        };
        renderer.exportTo(this->qwt_plot.get(), "plot.pdf", filesize, 300);
      }
  );

  qdoublevalidator.setRange(0e0, std::numeric_limits<double>::max(), 16);
  qdoublevalidator_no_min.setDecimals(16);

  // Adding reactions to choose from to reaction_selected_box
  QStringList reactions = this->mwstate->getReactions();
  for (int i = 0; i < reactions.size(); i++) {
    QString reaction = reactions.at(i);
    auto label = new QLabel(this);
    label->setText(reaction);
    label->setAlignment(Qt::AlignRight);

    auto preExpFac = new QLineEdit(this);
    preExpFac->setValidator(&qdoublevalidator);
    preExpFac->setText("0");
    preExpFactors[reaction] = preExpFac;

    auto actEner = new QLineEdit(this);
    actEner->setValidator(&qdoublevalidator);
    actEner->setText("0");
    activationEnergies[reaction] = actEner;

    auto label2 = new QLabel(this);
    label2->setText(reaction);
    label2->setAlignment(Qt::AlignRight);

    auto enthalpy = new QLineEdit(this);
    enthalpy->setValidator(&qdoublevalidator_no_min);
    enthalpy->setText("47e3");
    enthalpy->setMinimumWidth(60);
    enthalpy->setMaximumWidth(100);
    reactionsEnthalpy[reaction] = enthalpy;

    ui_temptab.reactions_values_Layout->addWidget(label,     i+1, 0);
    ui_temptab.reactions_values_Layout->addWidget(preExpFac, i+1, 1);
    ui_temptab.reactions_values_Layout->addWidget(actEner,   i+1, 2);

    ui_temptab.enthalpy_Layout->addWidget(label2,    i, 0);
    ui_temptab.enthalpy_Layout->addWidget(enthalpy,  i, 1);

    connect(preExpFac, SIGNAL(textEdited(QString)), parent, SLOT(modelModified()));
    connect(actEner,   SIGNAL(textEdited(QString)), parent, SLOT(modelModified()));
    connect(enthalpy,  SIGNAL(textEdited(QString)), parent, SLOT(modelModified()));
  }

  QStringList species = this->mwstate->getSpecies();
  for (int i = 0; i < species.size(); i++) {
    QString a_species = species.at(i);
    auto label = new QLabel(this);
    label->setText(a_species);
    label->setAlignment(Qt::AlignRight);

    auto flowBox = new QLineEdit(this);
    flowBox->setValidator(&qdoublevalidator);
    flowBox->setMaximumWidth(80);
    flowBox->setText("0");
    flowEntrySpecies[a_species] = flowBox;

    auto conInputSpecies = new QLineEdit(this);
    conInputSpecies->setValidator(&qdoublevalidator);
    conInputSpecies->setMaximumWidth(80);
    conInputSpecies->setText("0");
    concentrationInputSpecies[a_species] = conInputSpecies;

    ui_temptab.species_values_Layout->addWidget(label, i+1, 0);
    ui_temptab.species_values_Layout->addWidget(flowBox, i+1, 1);
    ui_temptab.species_values_Layout->addWidget(conInputSpecies, i+1, 2);

    connect(flowBox, SIGNAL(textEdited(QString)), parent, SLOT(modelModified()));
    connect(conInputSpecies, SIGNAL(textEdited(QString)), parent, SLOT(modelModified()));
  }

  ui_temptab.initial_temperature_box->setValidator(&qdoublevalidator);
  ui_temptab.temp_ambient_box->setValidator(&qdoublevalidator);
  ui_temptab.temp_species_box->setValidator(&qdoublevalidator);
  ui_temptab.kappa_box->setValidator(&qdoublevalidator);
  ui_temptab.heat_capacity_box->setValidator(&qdoublevalidator);

  // loading tab information
  if(auto tabInfo = tabInfo_) {
    ui_temptab.initial_temperature_box->setText(QString("%1").arg(tabInfo->value("initial temperature").toDouble()));
    ui_temptab.temp_ambient_box       ->setText(QString("%1").arg(tabInfo->value("ambient temperature").toDouble()));
    ui_temptab.temp_species_box       ->setText(QString("%1").arg(tabInfo->value("temperature entry species").toDouble()));
    ui_temptab.volume_reactor_box     ->setText(QString("%1").arg(tabInfo->value("volume reactor").toDouble()));
    ui_temptab.kappa_box              ->setText(QString("%1").arg(tabInfo->value("kappa").toDouble()));
    ui_temptab.heat_capacity_box      ->setText(QString("%1").arg(tabInfo->value("heat capacity").toDouble()));

    const QJsonObject &flowEntrySpeciesObject = tabInfo->value("flow entry species").toObject();
    const QJsonObject &concentrationInputObject = tabInfo->value("concentration entry species").toObject();
    for (auto&& it : flowEntrySpecies) {
      auto&& aSpecies = it.first;
      flowEntrySpecies[aSpecies]->setText(
          QString("%1").arg(flowEntrySpeciesObject[aSpecies].toDouble())
      );
      concentrationInputSpecies[aSpecies]->setText(
          QString("%1").arg(concentrationInputObject[aSpecies].toDouble())
      );
    }

    const QJsonObject &activationEnergiesObject = tabInfo->value("activation energy").toObject();
    const QJsonObject &preExpFactorsObject = tabInfo->value("pre-exponential factors").toObject();
    const QJsonObject &reactionsEnthalpyObject = tabInfo->value("reaction enthalpies").toObject();
    for (auto&& it : activationEnergies) {
      auto&& reaction = it.first;
      activationEnergies[reaction]->setText(
          QString("%1").arg(activationEnergiesObject[reaction].toDouble())
      );
      preExpFactors[reaction]->setText(
          QString("%1").arg(preExpFactorsObject[reaction].toDouble())
      );
      reactionsEnthalpy[reaction]->setText(
          QString("%1").arg(reactionsEnthalpyObject[reaction].toDouble())
      );
    }
  }

  // Connecting all static boxes to modelModified
  vector<QLineEdit*> fixedBoxes = {
    ui_temptab.temp_ambient_box,
    ui_temptab.temp_species_box,
    ui_temptab.kappa_box,
    ui_temptab.heat_capacity_box,
    ui_temptab.initial_temperature_box
  };
  for (auto&& box : fixedBoxes) {
    connect(box, SIGNAL(textEdited(QString)), parent, SLOT(modelModified()));
  }
}

//FlowTemperatureTab::~FlowTemperatureTab() {
//  std::cout << "Cleaning FlowTemperatureTab" << std::endl;
//}

void
FlowTemperatureTab::execute(const ReactionDetails &rd,
                        const SimulationDetails<double> &simuDetails) {

  FlowTemperature simulator(rd, simuDetails);

  simulator.setInitialTemp(    ui_temptab.initial_temperature_box->text().toDouble() );
  simulator.setTemp_ambient(   ui_temptab.temp_ambient_box->text().toDouble()        );
  simulator.setTemp_reactives( ui_temptab.temp_species_box->text().toDouble()        );
  simulator.setVolume_reactor( ui_temptab.volume_reactor_box->text().toDouble()      );
  simulator.setKappa(          ui_temptab.kappa_box->text().toDouble()               );
  simulator.setHeatCapacity(   ui_temptab.heat_capacity_box->text().toDouble()       );

  for (auto&& it : flowEntrySpecies) {
    simulator.setFlowEntrySpecies(it.first, it.second->text().toDouble());
    simulator.setConcentrationEntrySpecies(it.first, concentrationInputSpecies[it.first]->text().toDouble());
  }
  for (auto&& it : activationEnergies) {
    simulator.setActivationEnergy(it.first, it.second->text().toDouble());
    simulator.setPreExpFactor(it.first, preExpFactors[it.first]->text().toDouble());
    simulator.setReactionEnthalpy(it.first, reactionsEnthalpy[it.first]->text().toDouble());
  }

  // Running simulation
  runresult = simulator.run();

  qDebug("[temptab execute]: Time intervals %d", runresult->getNumTimeIntervals());

  saveDataAction->setEnabled(true);
  exportPlotAction->setEnabled(true);
  executed = true;

  auto species = runresult->getSpecies();
  species.emplace_back("Temperature");
  auto concents = runresult->getConcentrationsThruTime();
  auto timelabel = concents->at("Time");

  concentrations.clear();
  for (auto&& aSpecies : species) {
    QPolygonF points;
    int i = 0;
    for (auto&& conc: concents->at(aSpecies)) {
      points << QPointF(timelabel[i], conc);
      //qDebug() << timelabel[i] << " " << conc;
      i++;
    }

    concentrations.insert(QString::fromStdString(aSpecies), points);
  }

  replot();
}

bool
FlowTemperatureTab::isStillRunning() {
  return false;
}

void
FlowTemperatureTab::cleanTab() {
  curves.clear();
  concentrations.clear();
  saveDataAction->setEnabled(false);
  exportPlotAction->setEnabled(false);
  executed = false;
}

void
FlowTemperatureTab::reapplyBoxesState() {
  for(auto const& actEnerg : activationEnergies) {
    mwstate->setReactionsEnabled(actEnerg.first.toStdString(), false);
  }
}

void
FlowTemperatureTab::replot() {
  if (!executed) return;

  std::array<Qt::GlobalColor, 5> colors = {
    Qt::blue, Qt::red, Qt::green, Qt::magenta, Qt::gray
  };

  // Cleaning curves from plot. When a curve is deleted from memory it deletes
  // itself from the plot
  curves.clear();

  int j = 0;
  for (auto&& aSpecies : mwstate->getSpeciesToPlot()) {
    auto curve = std::make_unique<QwtPlotCurve>();
    curve->setTitle(aSpecies);
    curve->setPen(colors[j%5], 1),
    curve->setRenderHint(QwtPlotItem::RenderAntialiased, true);
    curve->setSamples(concentrations[aSpecies]);
    curve->attach(qwt_plot.get());

    curves.emplace(aSpecies, std::move(curve));
    j++;
  }

  auto curve = std::make_unique<QwtPlotCurve>();
  curve->setTitle("Temperature");
  curve->setYAxis(QwtPlot::yRight);
  curve->setPen(Qt::black, 1),
  curve->setRenderHint(QwtPlotItem::RenderAntialiased, true);
  curve->setSamples(concentrations["Temperature"]);
  curve->attach(qwt_plot.get());

  curves.emplace("Temperature", std::move(curve));

  qwt_plot->replot();
}

void
FlowTemperatureTab::when_save_data_is_pushed(bool checked) {
  qInfo() << "[FlowTemperatureTab]: Action triggered: Data Save";

  QString fileName;

  QFileDialog dialog(this);
  dialog.setAcceptMode(QFileDialog::AcceptSave);
  dialog.setFileMode(QFileDialog::AnyFile);
  dialog.setDirectory(QDir::homePath());
  dialog.setNameFilter(tr("Text File (*.txt)"));
  dialog.exec();

  if (dialog.selectedFiles().isEmpty()) return;

  fileName = dialog.selectedFiles().first();

  if (QFile::exists(fileName)) QFile::remove(fileName);

  QFile dataFile(fileName);
  if (dataFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
    QTextStream stream(&dataFile);
    //std::cout << runresult->toPlainText();
    stream << QString::fromStdString(runresult->toPlainText());
    dataFile.close();
  } else {
    QMessageBox::warning(
        this,
        "Failure saving file",
        "The file cannot be saved. "
        "Check whether you have the rights to access and modify the file.",
        QMessageBox::Ok,
        QMessageBox::NoButton
    );
  }
}

QJsonObject
FlowTemperatureTab::toJsonObject() {
  QJsonObject flowEntrySpeciesObject;
  QJsonObject concentrationInputObject;
  QJsonObject activationEnergiesObject;
  QJsonObject preExpFactorsObject;
  QJsonObject reactionsEnthalpyObject;

  for (auto&& it : flowEntrySpecies) {
    flowEntrySpeciesObject.insert(it.first, it.second->text().toDouble());
    concentrationInputObject.insert(it.first, concentrationInputSpecies[it.first]->text().toDouble());
  }
  for (auto&& it : activationEnergies) {
    const QString &reaction = it.first;
    activationEnergiesObject.insert(reaction, it.second->text().toDouble());
    preExpFactorsObject.insert(reaction, preExpFactors[reaction]->text().toDouble());
    reactionsEnthalpyObject.insert(reaction, reactionsEnthalpy[reaction]->text().toDouble());
  }

  return QJsonObject {
    {"type", "temperature"},
    {"initial temperature"        , ui_temptab.initial_temperature_box->text().toDouble()},
    {"ambient temperature"        , ui_temptab.temp_ambient_box->text().toDouble()},
    {"temperature entry species"  , ui_temptab.temp_species_box->text().toDouble()},
    {"volume reactor"             , ui_temptab.volume_reactor_box->text().toDouble()},
    {"kappa"                      , ui_temptab.kappa_box->text().toDouble()},
    {"heat capacity"              , ui_temptab.heat_capacity_box->text().toDouble()},
    {"flow entry species"         , flowEntrySpeciesObject },
    {"concentration entry species", concentrationInputObject },
    {"activation energy"          , activationEnergiesObject },
    {"pre-exponential factors"    , preExpFactorsObject },
    {"reaction enthalpies"        , reactionsEnthalpyObject},
  };
}
