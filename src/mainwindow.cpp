/*
 * Copyright 2017-2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mainwindow.h"

#include <QAction>
#include <QCheckBox>
#include <QCloseEvent>
#include <QDebug>
#include <QDialog>
#include <QDirIterator>
#include <QDoubleValidator>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QInputDialog>
#include <QJsonObject>
#include <QJsonValue>
#include <QLineEdit>
#include <QMessageBox>
#include <QPair>
#include <QStandardItemModel>
#include <QTableView>
//#include <QThread> // to use with QThread::sleep(secs)
#include <QWizard>
#include <qwt_mathml_text_engine.h>
#include <qwt_text.h>

#include <iostream>
#include <limits>
#include <tuple>

#include "optional.hpp"

#include "factorytab.h"
#include "mainwindowstate.h"
#include "qt_version_compatibility.h"
#include "saverloader.h"
#include "ui/dialogmatrices.h"
#include "ui/newreactionswizard.h"

/*
 *class MyItemDelegate : public QItemDelegate {
 *    public:
 *        QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
 *};
 */

using std::experimental::make_optional;
using std::experimental::optional;
using std::make_pair;
using std::make_unique;
using std::shared_ptr;
using std::string;
using std::tuple;
using std::unique_ptr;
using std::unordered_map;
using std::vector;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      ui               (make_unique<Ui::MainWindow>()),
      qdoublevalidator (make_unique<QDoubleValidator>(this))
{
  qDebug() << QCoreApplication::arguments();

  ui->setupUi(this);
  this->resize(950,560);

  qdoublevalidator->setRange(0e0, std::numeric_limits<double>::max(), 16);

  shared_ptr<QLineEdit>
    Tmax_box_smart  (shared_ptr<QLineEdit>(), ui->Tmax_box ),
    Delta_box_smart (shared_ptr<QLineEdit>(), ui->Delta_box),
    ATOL_box_smart  (shared_ptr<QLineEdit>(), ui->ATOL_box ),
    RTOL_box_smart  (shared_ptr<QLineEdit>(), ui->RTOL_box );

  simuDetailsBoxes = std::make_shared< SimulationDetails<shared_ptr<QLineEdit>> >(
     Tmax_box_smart,
     Delta_box_smart,
     ATOL_box_smart,
     RTOL_box_smart
  );

  // Adding examples to list of examples
  fillExamplesMenu();

  tuple<shared_ptr<ReactionDetails>,
        shared_ptr<SimulationDetails<double>>,
        shared_ptr<vector<QJsonObject>>,
        QStringList>
      rds = SaverLoader::defaultReactionsAndSimulationDetls();

  //simulationDetails = std::make_shared<SimulationDetails<double>>(0, 240000, 100, 1e-15, 1e-15);
  //qDebug() << simulationDetails->getT_0();

  // Defining MathMLText to display equations
  QwtText::setTextEngine( QwtText::MathMLText, new QwtMathMLTextEngine() );

  reloadBasicInterfaceUsing(std::get<0>(rds), std::get<1>(rds), std::get<2>(rds), std::get<3>(rds));

  ui->tabWidget->setTabsClosable(true);

  // Creating Actions in the menu add_tab_menu for the user to add new tabs
  QMap<QString, QString> tabTypesNames = FactoryTab::tabTypes();
  tabTypesNames.remove("timeseries"); // ignoring the time series tab, there should be only one of its type

  for (auto&& tabtypename : tabTypesNames.toStdMap()) {
    QString tabtype = tabtypename.first;
    QString tabname = tabtypename.second;

    auto actionNewTab = new QAction(tabname, ui->add_tab_menu);
    ui->add_tab_menu->addAction(actionNewTab);

    connect(actionNewTab, &QAction::triggered,
        [=](bool checked) {
          auto newTab = FactoryTab::getTabPtr(tabtype, this->getMWState(), this);
          if(newTab.has_value()) {
            this->addTab(tabname, newTab.value());
            setWindowModified(true);
          } else {
            qDebug() << "There is no tab of type" << tabtype;
          }
        }
    );
  }

  // Connecting actions to respective slots
  connect(ui->actionnew_reactions,     SIGNAL(triggered()), this, SLOT(when_new_reactions_is_pushed()));
  connect(ui->actionmodify_reactions,  SIGNAL(triggered()), this, SLOT(when_modify_reactions_is_pushed()));
  connect(ui->actionload_reactions,    SIGNAL(triggered()), this, SLOT(when_load_reactions_is_pushed()));
  connect(ui->actionsave_reactions,    SIGNAL(triggered()), this, SLOT(when_save_reactions_is_pushed()));
  connect(ui->actionshow_SNA_matrices, SIGNAL(triggered()), this, SLOT(when_show_SNA_matrices()));
  connect(ui->actionabout,             SIGNAL(triggered()), this, SLOT(when_about()));
  connect(ui->actionabbr ,             SIGNAL(triggered()), this, SLOT(when_abbreviation()));
  connect(ui->tabWidget, SIGNAL(tabCloseRequested(int)), this, SLOT(removeTab(int)));

  ui->actionquit->setShortcut(Qt::Key_Q | Qt::CTRL);
  connect(ui->actionquit, SIGNAL(triggered()), this, SLOT(close()));

  // When the current displayed tab is changed all the behaivor associated to it should also change
  connect(ui->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(reloadTabActionsAndRestrictions(int)) );

  // Connecting all static boxes to modelModified
  vector<QLineEdit*> fixedBoxes = {
    ui->Tmax_box, ui->Delta_box, ui->ATOL_box, ui->RTOL_box
  };
  for (auto&& box : fixedBoxes) {
    connect(box, SIGNAL(textEdited(QString)), this, SLOT(modelModified()));
  }
}

MainWindow::~MainWindow() {
  //std::cout << "Cleaning MainWindow" << std::endl;
  disconnect(ui->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(reloadTabActionsAndRestrictions(int)) );
  //std::cout << "Window size " << this->size().width() << " " << this->size().height() << std::endl;
}

void MainWindow::fillExamplesMenu() {
  QString examples_dir(":/examples");
  QDirIterator examples(examples_dir, QDir::Files | QDir::Readable, QDirIterator::Subdirectories);

  if(! examples.hasNext()) {
    ui->menu_example_reactions->setEnabled(false);
  } else {
    QDir exs_dir(examples_dir);
    QList<QPair<QFileInfo, QStringList>> exs_positions;
    while (examples.hasNext()) {
      QFileInfo ex(examples.next());
      //qDebug() << ex.baseName();
      //qDebug() << ex.filePath();

      QString rel_path(exs_dir.relativeFilePath(ex.path()));
      //qDebug() << "Relative path: " << rel_path;
      QStringList submenu_path;
      if (rel_path != ".") {
        submenu_path = QDir::toNativeSeparators(rel_path).split(QDir::separator(), QString::SkipEmptyParts);
      }
      //qDebug() << submenu_path.size();
      // no naked pointer should be used, but I'm not quite sure how Qt manages pointers, I'm hoping Qt
      // deletes it once the parent is also deleted (TODO: search how Qt manages pointers!!)
      exs_positions.append(qMakePair(ex, submenu_path));
    }

    QMap<QString, QMenu*> menus;
    for (auto&& ex : exs_positions) {
      // Creating and connecting action
      auto openFileAction = new QAction(this);
      openFileAction->setText( ex.first.baseName() );
      connect(openFileAction, &QAction::triggered,
          [=](bool checked) {
            if(!maybeSaveAndKeepOn())
              return;
            this->loadInterfaceFromFile( ex.first.filePath() );
          }
      );

      // Adding action to the menu
      QMenu* menu = ui->menu_example_reactions;
      QStringList submenu_path = ex.second;
      QString menuname = submenu_path.join('/');
      if(ex.second.size() > 0) {
        if(menus.contains(menuname)) {
          menu = menus[menuname];
        } else {
          QString menuiter(submenu_path[0]);
          int i = 0;

          while (menuname != menuiter) {
            if(! menus.contains(menuiter)) {
              menus[menuiter] = menu->addMenu(submenu_path[i]);
            }
            menu = menus[menuiter];

            i++;
            menuiter += '/';
            menuiter += submenu_path[i];
          }

          menu = menus[menuiter] = menu->addMenu(submenu_path[i]);
        }
      }
      // Selecting adecuate submenu
      menu->addAction(openFileAction);
    }
  }
}

/*
 * Steps followed in Reloading the User interface:
 * given ReactionDetails and SimulationDetails
 * (TODO: more info in the future, e.g., dialogs)
 *
 * Deleting
 * 1) delete Tabs
 * 2) delete dialogs
 * 3) delete boxes
 * 4) delete old ReactionDetails and SimulationDetails
 * Adding
 * 5) create new boxes
 * 6) setting values given in rd and simuDetails
 * 7) create new dialogs
 * 8) create QCheckBoxes for species to plot
 * 9) create tabs
 * 10) set window title
 */
void
MainWindow::reloadBasicInterfaceUsing(
							shared_ptr<ReactionDetails> rd,
				      shared_ptr<SimulationDetails<double>> simuDetails,
              shared_ptr<vector<QJsonObject>> tabs,
              optional<const QStringList &> speciesToPlot,
              optional<const QString &> title) {

  if( ! rd ) {
    qDebug() << "Error: ReactionDetails is empty!!!";
    return;
  }
  if( ! simuDetails ) {
    qDebug() << "Error: SimulationDetails is empty!!!";
    return;
  }

  // 1) Cleaning tab interface
  //qDebug() << "Cleaning Tab Interface";
  ui->tabWidget->clear();
  windowTabs.clear();

  // 2) Delete dialogs
  dialogsToShow.clear();

  // 3) deleting all boxes
  // Deleting all Boxes
  if (ui->concentrationsLayout->count() > 0) {
    // Workaround to "delete" rows
    while (QLayoutItem *item = ui->concentrationsLayout->takeAt(0)) {
      item->widget()->deleteLater();
      delete item;
    }
  }

  if (ui->ratesLayout->count() > 0) {
    while (QLayoutItem *item = ui->ratesLayout->takeAt(0)) {
      item->widget()->deleteLater();
      delete item;
    }
  }
  reactionsBoxes.reset();
  speciesBoxes.reset();
  speciesToPlotCheckBoxes.clear();

  // 4) deleting old ReactionDetails and SimulationDetails
  reactionDetails.reset();
  simulationDetails.reset();

  // *) Interlude
  qDebug() << "Entered reactions";
  qDebug("%s", rd->reactionsAsQString().toUtf8().data());
  qDebug() << "Stoichiometric Matrix";
  qDebug("%s", rd->stoichiometricMatrixAsQString().toUtf8().data());
  qDebug() << "Reactions Order Matrix";
  qDebug("%s", rd->reactionsOrderMatrixAsQString().toUtf8().data());

  // 5) creating new boxes
  reactionsBoxes = std::make_shared< unordered_map<string, shared_ptr<QLineEdit>> >();
  speciesBoxes   = std::make_shared< unordered_map<string, shared_ptr<QLineEdit>> >();

  // Creating Boxes to hold values for the species and setting them
  for ( const QString &aSpeciesQt : rd->getSpecies() ) {
    auto newBox = std::make_shared<QLineEdit>(this);
    QString initialConcentration = QString::number(rd->getInitialConcentration(aSpeciesQt), 'g', 16);

    newBox->setValidator(qdoublevalidator.get());
    newBox->setPlaceholderText(initialConcentration);
    newBox->setText(initialConcentration);
    newBox->setMaximumWidth(80);

    ui->concentrationsLayout->addRow(aSpeciesQt, newBox.get());
    speciesBoxes->emplace(aSpeciesQt.toStdString(), newBox);
    connect(newBox.get(), SIGNAL(textEdited(QString)), this, SLOT(modelModified()));
  }

  // Creating Boxes to hold values for the reactions rates and setting them
  for ( const QString &reactionQt: rd->getReactions() ) {
    auto newBox = std::make_shared<QLineEdit>(this);
    QString rate = QString::number(rd->getRate(reactionQt), 'g', 16);

    newBox->setValidator(qdoublevalidator.get());
    newBox->setPlaceholderText(rate);
    newBox->setText(rate);
    newBox->setMinimumWidth(60);
    newBox->setMaximumWidth(80);

    ui->ratesLayout->addRow(reactionQt, newBox.get());
    reactionsBoxes->emplace(reactionQt.toStdString(), newBox);

    connect(newBox.get(), SIGNAL(textEdited(QString)), this, SLOT(modelModified()));
  }

  // 6) Writing values of the simulation in boxes
  QPair<QLineEdit*, QString> qLineEdits[] = {
      qMakePair( ui->Tmax_box,  QString::number(simuDetails->getTmax(),  'g', 16) ),
      qMakePair( ui->Delta_box, QString::number(simuDetails->getDelta(), 'g', 16) ),
      qMakePair( ui->ATOL_box,  QString::number(simuDetails->getATOL(),  'g', 16) ),
      qMakePair( ui->RTOL_box,  QString::number(simuDetails->getRTOL(),  'g', 16) )
  };

  for (QPair<QLineEdit*, QString> qline : qLineEdits) {
    qline.first->setValidator(qdoublevalidator.get());
    qline.first->setPlaceholderText(qline.second);
    qline.first->setText(qline.second);
  }

  // 7)
  //dialogsToShow = std::make_unique< unordered_map<string, unique_ptr<QDialog>> >();
  //dialogsToShow =  shared_ptr< unordered_map<string, shared_ptr<QDialog>> >(
  //      new unordered_map<string, shared_ptr<QDialog>>(),
  //      [](unordered_map<string, shared_ptr<QDialog>>* ptr){
  //        std::cout << "Cleaning dialogsToShow" << std::endl;
  //        delete ptr;
  //      }
  //  );
  dialogsToShow.emplace("matricesDialog", createMatricesDialog(*rd));

  this->reactionDetails   = rd;
  this->simulationDetails = simuDetails;

  // 8) Adding checked boxes of species to print
  qDebug() << "Species to Plot";

  bool areSpeciesDetermined = speciesToPlot.has_value();
  if(areSpeciesDetermined) {
    qDebug() << "[As defined in JSON]";
  }
  QStringList species = rd->getSpecies();
  for (int i = 0; i < species.size(); i++) {
    QString a_species = species.at(i);
    auto speciesBox = std::make_shared<QCheckBox>(a_species);

    speciesToPlotCheckBoxes.push_back(speciesBox);
    ui->selectSpeciesLayout->addWidget( speciesBox.get() );

    bool plot;
    if(areSpeciesDetermined) {
      plot = speciesToPlot.value().contains(a_species);
    } else {
      plot = true;
    }

    speciesBox->setChecked(plot);
    if(plot) {
      qDebug() << a_species;
    }

    connect(speciesBox.get(), SIGNAL(stateChanged(int)), this, SLOT(modelModified()));
    connect(speciesBox.get(), SIGNAL(stateChanged(int)), this, SLOT(replot()));
  }

  // 9) creating new tabs and reloading tab actions

  //qDebug() << "Creating Tabs";
  if(!tabs) {
    auto tab = FactoryTab::defaultTab(getMWState(), this);
    addTab(tab.first, tab.second);
  } else {
    for (const QJsonObject& tabObject : *tabs) {
      QString tabtype = tabObject.value("type").toString();
      QString tabName = tabObject.value("name").toString();

      auto tabptr = FactoryTab::getTabPtr(tabtype, getMWState(), this, make_optional(tabObject));

      if (tabptr.has_value()) {
        addTab(tabName, tabptr.value());
      } else {
        qDebug() << QString("Error: tab type `%1' unknown").arg(tabtype);
      }
    }
  }
  reloadTabActionsAndRestrictions(0);

  // 10) Setting Window title
  if(title.has_value()) {
    setWindowTitle(QString("%1[*] - ChemKinLator").arg(title.value()));
  } else {
    setWindowTitle("[*]ChemKinLator");
  }

  setWindowModified(false);
}

void
MainWindow::loadInterfaceFromFile(QString fileName) {
  if( !QFile::exists(fileName) ) {
    // TODO: display dialog telling the user the file wasn't found
    qInfo() << "File" << fileName << "doesn't exists";
    return;
  }

  QFile file( fileName );
  optional<std::tuple<
    std::shared_ptr<ReactionDetails>,
    std::shared_ptr<SimulationDetails<double>>,
    std::shared_ptr<vector<QJsonObject>>,
    QStringList
  >> rds;
  // QIODevice::WriteOnly truncates an already written file
  if ( file.open(QIODevice::ReadOnly) ) {
    rds = SaverLoader::fromJson(QTextStream(&file).readAll());
    file.close();
  }

  // if rds contains something
  if(auto rds_ = rds) {
    QFileInfo finfo(fileName);
    QString basename(finfo.fileName());
    reloadBasicInterfaceUsing(
        std::get<0>(*rds_),
        std::get<1>(*rds_),
        std::get<2>(*rds_),
        std::get<3>(*rds_),
        basename
    );
  }
}

void
MainWindow::addTab(const QString &tabName, shared_ptr<MainWindowTab> tab) {
  ui->tabWidget->addTab(tab.get(), tabName);
  windowTabs.emplace_back(tabName, tab);
}

unique_ptr<MainWindowState>
MainWindow::getMWState() {
  return std::make_unique<MainWindowState>(
    reactionDetails->getSpecies(),
    reactionDetails->getReactions(),
    speciesBoxes,
    reactionsBoxes,
    simuDetailsBoxes,
    speciesToPlotCheckBoxes
  );
}

void
MainWindow::on_pushButton_clicked() {
  // Checking whether any tab is running
  for (auto&& tabPair : windowTabs) {
    if( tabPair.second->isStillRunning() ) {
        qDebug() << "Some tab is still running. ";
        return;
    }
  }

  this->copyInfoToReactionAndSimulationDetails();

  auto tab = windowTabs.at( ui->tabWidget->currentIndex() ).second;
  tab->cleanTab();
  tab->execute(*reactionDetails, *simulationDetails);
}

void
MainWindow::copyInfoToReactionAndSimulationDetails() {
  // Setting initial reactions to reactionDetails
  for ( const QString &aSpeciesQt: reactionDetails->getSpecies() ) {
    string aSpecies = aSpeciesQt.toStdString();
    double newInitialConcentration = speciesBoxes->at(aSpecies)->text().toDouble();
    reactionDetails->setInitialConcentration( aSpeciesQt, newInitialConcentration );
  }

  // Setting reactions rates to reactionDetails
  for ( const QString &reactionQt: reactionDetails->getReactions() ) {
    string reaction = reactionQt.toStdString();
    double newRate = reactionsBoxes->at(reaction)->text().toDouble();
    reactionDetails->setRate( reactionQt, newRate );
  }

  // Setting simulation values
  simulationDetails->setTmax  ( ui->Tmax_box ->text().toDouble() );
  simulationDetails->setDelta ( ui->Delta_box->text().toDouble() );
  simulationDetails->setATOL  ( ui->ATOL_box ->text().toDouble() );
  simulationDetails->setRTOL  ( ui->RTOL_box ->text().toDouble() );
}

void
MainWindow::when_new_reactions_is_pushed() {

  qInfo() << "Action triggered: Insert new reaction network";

  if(!maybeSaveAndKeepOn())
    return;

  Ui::NewReactionsWizard wizardWindow( this );
  wizardWindow.exec();

  // if the cancel button was pressed then we receive no reactions
  optional<QStringList> listOfReactions = wizardWindow.getReactions();

  if( listOfReactions.has_value() ) {
    qDebug() << "Reactions entered:" << listOfReactions.value();

    auto rd          = std::make_shared<ReactionDetails>(listOfReactions.value());
    auto simuDetails = std::make_shared<SimulationDetails<double>>(240000, 100, 1e-15, 1e-15);

    reloadBasicInterfaceUsing(rd, simuDetails);
  }

  setWindowModified(true);
}

void
MainWindow::when_modify_reactions_is_pushed() {

  qInfo() << "Action triggered: Modify reaction network";

  QString msg = "You are about to modify the current reaction network.\n"
                "You will lose all values.\n"
                "Do you wish to save the current reaction network/model?";
  if(!maybeSaveAndKeepOn(msg))
    return;

  Ui::NewReactionsWizard wizardWindow(this, reactionDetails->getReactions());
  wizardWindow.exec();

  // if the cancel button was pressed then we receive no reactions
  optional<QStringList> listOfReactions = wizardWindow.getReactions();

  if( listOfReactions.has_value() ) {
    qDebug() << "Reactions entered:" << listOfReactions.value();

    auto rd          = std::make_shared<ReactionDetails>(listOfReactions.value());
    auto simuDetails = std::make_shared<SimulationDetails<double>>(240000, 100, 1e-15, 1e-15);

    reloadBasicInterfaceUsing(rd, simuDetails);
  }

  setWindowModified(true);
}

void
MainWindow::when_load_reactions_is_pushed() {

  qInfo() << "Action triggered: Load reaction network";

  if(!maybeSaveAndKeepOn())
    return;

  QFileDialog dialog(this);
  dialog.setAcceptMode(QFileDialog::AcceptOpen);
  dialog.setFileMode(QFileDialog::AnyFile);
  dialog.setDirectory(QDir::homePath());
  dialog.setNameFilter(tr("JSON (*.simu.json)"));
  dialog.exec();

  // don't do anything else, the user hasn't chosen any file
  if( dialog.selectedFiles().isEmpty() ) { return; }

  loadInterfaceFromFile( dialog.selectedFiles().first() );
}

void
MainWindow::when_save_reactions_is_pushed() {

  qInfo() << "Action triggered: Save reaction network";

  this->copyInfoToReactionAndSimulationDetails();

  QFileDialog dialog(this);
  dialog.setAcceptMode(QFileDialog::AcceptSave);
  dialog.setFileMode(QFileDialog::AnyFile);
  dialog.setDirectory(QDir::homePath());
  dialog.setNameFilter(tr("JSON (*.simu.json)"));
  dialog.setDefaultSuffix("simu.json");
  dialog.exec();

  if( dialog.selectedFiles().isEmpty() ) { return; }

  QString fileName = dialog.selectedFiles().first();

  if (QFile::exists(fileName)) { QFile::remove(fileName); }
  QFile file(fileName);
  // QIODevice::WriteOnly truncates an already written file
  if (file.open(QIODevice::WriteOnly)) {
    vector<QJsonObject> tabs;
    for (auto&& tabPair : windowTabs) {
      QJsonObject tabObject = tabPair.second->toJsonObject();
      tabObject.insert( "name", tabPair.first );
      tabs.emplace_back( tabObject );
    }

    QStringList speciesToPlot;
    for (auto&& checkbox : speciesToPlotCheckBoxes) {
      if(checkbox->isChecked()) {
        speciesToPlot << checkbox->text();
      }
    }

    QTextStream out(&file);
    out << SaverLoader::toJson(*reactionDetails, *simulationDetails, tabs, speciesToPlot);
    file.close();

    setWindowModified(false);
  }
}

void
MainWindow::when_show_SNA_matrices() {
  if (dialogsToShow.count("matricesDialog") > 0) {
    dialogsToShow.at("matricesDialog")->show();
  } else {
    qDebug() << "No matricesDialog found!! :S";
  }
}

void
MainWindow::when_about() {
  QMessageBox::about(
    this,
    "About ChemKinLator",
    "Chemical Kinetics Simulator\n"
    "Licensed under Apache 2.0\n"
    "\n"
    "(c) 2017-2019\n"
    "Universidad Nacional de Colombia\n"
    "\n"
    "Libraries:\n"
    "- Qt5\n"
    "- Chemkinlator is based in part on the work of the Qwt project (http://qwt.sf.net)."
  );
}

void
MainWindow::when_abbreviation() {
  QMessageBox msg (
    QMessageBox::Information,
    "Abbreviations and other infomation",
    "The following are the currently used abbreviations by ChemKinLator:"
    "<br/><br/>"
    "<b>t<sub>max</sub></b>: "
    "Maximum time for simulation.<br/>"
    "<b>kappa</b>: Global heat transfer constant of the system.<br/>"
    "<b>pre-exp</b>: Pre-exponential Factor of the Arrhenius equation.<br/>"
    "<b>ATOL</b>: Absolute tolerance parameter. See more details in the "
    "comments in the `dlsode.for` file.<br/>"
    "<b>RTOL</b>: Relative tolerance parameter.<br/>"
    "<br/>"
    "<b>Note about units</b>:<br/>"
    "The interface does not force on the user any kind of units.<br/>"
    "All results should be interpreted by the units assumed by the user of the application.<br/>"
    "For example: if the user assumes that the input time is in seconds, then the plot will "
    "show the result of a simulation in seconds.<br/>"
    "All units used in the examples are in the SI international system of "
    "units. The consistency of the units in any other model used in Chemkinlator "
    "falls on the user. Be sure to use a consistent set of units or a whole "
    "dimensionless set of values.<br/>",
    QMessageBox::Ok,
    this
  );
  msg.setTextFormat(Qt::RichText);
  msg.exec();
}

void
MainWindow::when_rename_tab_is_pushed() {
  int i = ui->tabWidget->currentIndex();

  QInputDialog inputdialog(this);
  inputdialog.setWindowTitle("Rename Tab");
  inputdialog.setWindowTitle("Write new name for tab:");
  inputdialog.setInputMode(QInputDialog::TextInput);

  // Enabling only alphanumeric tab titles
  QLineEdit *line = inputdialog.findChild<QLineEdit*>();
  auto val = new QRegExpValidator(QRegExp(R"((\w|\s)*)"), this);
  line->setText(windowTabs[i].first);
  line->setValidator(val);

  auto ret = inputdialog.exec();
  QString newname = line->text();

  if (ret == QDialog::Accepted && !newname.isEmpty()) {
    ui->tabWidget->setTabText(i, newname);
    windowTabs[i] = make_pair(newname, windowTabs[i].second);
    modelModified();
  }
}

/*
 *QWidget *MyItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const {
 *  qDebug() << index.data().toString();
 *  //if( index.data() != QString("") && index.data() != QString("a")) {
 *      //return NULL; // Prevent editing
 *  //}
 *  return QItemDelegate::createEditor(parent, option, index);
 *}
 */

unique_ptr<QDialog>
MainWindow::createMatricesDialog(const ReactionDetails &rd) {

  auto matricesDialog = std::make_unique<Ui::QDialogMatrices>(this);
  matricesDialog->setWindowTitle("Differential Equations and SNA Matrices");

  // Creating labels
  matricesDialog->addItem("Reactions", rd.reactionsAsQString());
  matricesDialog->addItem("Differential Equations", rd.diffEquationsMathML(), true);
  matricesDialog->addItem("Stoichiometric Matrix",  rd.stoichiometricMatrixAsQString());
  matricesDialog->addItem("Order Reactions Matrix", rd.reactionsOrderMatrixAsQString());
  matricesDialog->addItem("Differential Equations with Temperature", rd.diffEquationsMathML(true), true);

  // std::move isn't necessary, it's just there for a legacy compiler (g++-4.9)
  return std::move(matricesDialog);
}

void
MainWindow::reloadTabActionsAndRestrictions(int index) {

  //qDebug("INSIDE RELOAD TAB ACTIONS");
  //qDebug() << "index" << index;
  //if(windowTabs) qDebug() << "windowTabs == true";
  //qDebug() << "windowTabs->size()" << windowTabs->size();
  //if(reactionsBoxes)   qDebug() << "reactionsBoxes == true";
  //if(speciesBoxes)     qDebug() << "speciesBoxes == true";
  //if(simuDetailsBoxes) qDebug() << "simuDetailsBoxes == true";
  if(index!=-1 // there is at least a tab in tabWidget
      && ! windowTabs.empty()
      && index < static_cast<int>(windowTabs.size()) // index isn't bigger than the saved tabs
      && reactionsBoxes // checking if it isn't nullptr. TODO: this should be changed to asserts
      && speciesBoxes
      && simuDetailsBoxes
  ) {
    //qDebug("Cleaning Tab Menu");
    ui->tab_menu->clear();

    //qDebug("Adding `Rename Tab` action");
    auto renametab = new QAction(ui->tab_menu);
    renametab->setText("Rename Tab");
    ui->tab_menu->addAction( renametab );
    connect(renametab, SIGNAL(triggered()), this, SLOT(when_rename_tab_is_pushed()));

    //qDebug("Adding `Add Tab` menu");
    ui->tab_menu->addMenu( ui->add_tab_menu );
    //qDebug("Adding actions from tab");

    auto actionsForTab = windowTabs.at(index).second->getActions();
    for (shared_ptr<QAction> action: actionsForTab) {
      ui->tab_menu->addAction( action.get() );
    }

    getMWState()->enableAllBoxes();
    // asking tab to hide whatever it wants to hide
    windowTabs.at(index).second->reapplyBoxesState();
  }
  //qDebug("OUT OF RELOAD TAB ACTIONS");
}

void
MainWindow::closeEvent(QCloseEvent *event) {

    if (maybeSaveAndKeepOn()) {
        event->accept();
    } else {
        event->ignore();
    }
}

void
MainWindow::modelModified() {
  setWindowModified(true);
}

void
MainWindow::replot() {
  auto tab = windowTabs.at( ui->tabWidget->currentIndex() ).second;

  if( tab->isStillRunning() ) {
      return;
  }

  tab->replot();
}

void
MainWindow::removeTab(int i) {
  if (i == 0) {
    QMessageBox::information(
        this,
        "Trying to close first tab",
        "The first tab (Time Series) cannot be closed"
    );
  } else {
    ui->tabWidget->removeTab(i);
    windowTabs.erase(windowTabs.begin()+i);
    modelModified();
  }
}

bool
MainWindow::maybeSaveAndKeepOn(const QString &msg_) {
  if(!isWindowModified())
    return true;

  auto msg = msg_.size()
           ? msg_
           : "The model hasn't been saved.\n"
             "Do you wish to save it before continuing?";

  auto reply = QMessageBox::question(
      this,
      "Model modified",
      msg,
      QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel
  );

  switch (reply) {
    case QMessageBox::Yes:
      when_save_reactions_is_pushed();
      return true;
    case QMessageBox::No:
      return true;
    case QMessageBox::Cancel:
      return false;
    default:
      // THIS LINE SHOULD NEVER BE RUN!!
      return false;
  }
}
