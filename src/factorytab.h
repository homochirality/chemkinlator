/*
 * Copyright 2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <QJsonObject>
#include <QMap>
#include <QWidget>
#include <memory>
#include <utility>

#include "mainwindowstate.h"
#include "tabs/mainwindowtab.h"

#include "optional.hpp"

#ifndef SRC_FACTORYTAB_H_
#define SRC_FACTORYTAB_H_

namespace FactoryTab {

std::pair<QString, std::shared_ptr<MainWindowTab>>
  defaultTab(
    std::unique_ptr<MainWindowState> mwstate,
    QWidget *widget
  );

std::experimental::optional<std::shared_ptr<MainWindowTab>>
  getTabPtr(
      QString tabtype,
      std::unique_ptr<MainWindowState> mwstate,
      QWidget *widget,
      const std::experimental::optional<QJsonObject> & tabInfo = std::experimental::nullopt
  );

QMap<QString, QString> tabTypes();

} // namespace FactoryTab

#endif  // SRC_FACTORYTAB_H_
