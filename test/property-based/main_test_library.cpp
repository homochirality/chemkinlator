/*
 * Copyright 2017 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <rapidcheck.h>

#include <array>

#include "simulationdetails.h"

int main() {
  bool everything_ok = true;

  everything_ok = everything_ok &&
  rc::check("SimulationDetails must return what it was given",
            //[](const std::array<double,5> &l0) {               // more readable version, using C++17 new features
            //  const auto [Tmax, Delta, ATOL, RTOL] = l0;
            [](const double &Tmax, const double &Delta, const double &ATOL, const double &RTOL) {
              SimulationDetails<double> simuDetails {Tmax, Delta, ATOL, RTOL};
              RC_ASSERT( Tmax  == simuDetails.getTmax()  );
              RC_ASSERT( Delta == simuDetails.getDelta() );
              RC_ASSERT( ATOL  == simuDetails.getATOL()  );
              RC_ASSERT( RTOL  == simuDetails.getRTOL()  );
            });

  return everything_ok ? 0 : 1;
}
