TARGET = chemkinlator

QMAKE_CXXFLAGS += -std=c++14

CONFIG += qt static staticlib c++14
QT += widgets svg xml opengl concurrent printsupport

LIBS += -lgfortran -lquadmath

RESOURCES += examples/examples.qrc

win32 {
  DEFINES += WINDOWS_FLAG
}

INCLUDEPATH += \
  lib/c++ \
  lib/c++/optional \
  src \
  lib/qwt-mml-dev \
  lib/qwt-6.1/src

HEADERS += src/mainwindow.h \
           src/mainwindowstate.h \
           src/qt_version_compatibility.h \
           src/reactiondetails.h \
           src/saverloader.h \
           lib/c++/simulationdetails.h \
           lib/c++/simulationresults.h \
           src/simulators/simulator.h \
           src/simulators/flow_temperature.h \
           src/simulators/time_series.h \
           src/ui/dialogmatrices.h \
           src/factorytab.h \
           src/tabs/bifurcationtab.h \
           src/tabs/flowtemperaturetab.h \
           src/tabs/timeseriestab.h \
           src/tabs/mainwindowtab.h \
           src/ui/newreactionswizard.h \
           lib/qwt-mml-dev/qwt_mathml_text_engine.h \
           lib/qwt-mml-dev/qwt_mml_document.h \
           lib/qwt-mml-dev/qwt_mml_entity_table.h

SOURCES += src/main.cpp \
           src/mainwindow.cpp \
           src/mainwindowstate.cpp \
           src/reactiondetails.cpp \
           src/saverloader.cpp \
           lib/c++/simulationresults.cpp \
           src/simulators/simulator.cpp \
           src/simulators/flow_temperature.cpp \
           src/simulators/time_series.cpp \
           src/ui/dialogmatrices.cpp \
           src/factorytab.cpp \
           src/tabs/bifurcationtab.cpp \
           src/tabs/flowtemperaturetab.cpp \
           src/tabs/timeseriestab.cpp \
           src/ui/newreactionswizard.cpp \
           lib/fortran/chemical_simulator.F90 \
           lib/fortran/chemical_simulator_with_temp.F90 \
           lib/fortran/dlsode.for \
           lib/qwt-mml-dev/qwt_mathml_text_engine.cpp \
           lib/qwt-mml-dev/qwt_mml_document.cpp \
           lib/qwt-mml-dev/qwt_mml_entity_table.cpp

FORMS += \
        src/ui/mainwindow.ui \
        src/tabs/ui/bifurcationtab.ui \
        src/tabs/ui/flowtemperaturetab.ui

HEADERS += \
    lib/qwt-6.1/src/qwt.h \
    lib/qwt-6.1/src/qwt_global.h \
    lib/qwt-6.1/src/qwt_text_engine.h \
    lib/qwt-6.1/src/qwt_text_label.h \
    lib/qwt-6.1/src/qwt_text.h \
    lib/qwt-6.1/src/qwt_painter.h \
    lib/qwt-6.1/src/qwt_math.h \
    lib/qwt-6.1/src/qwt_clipper.h \
    lib/qwt-6.1/src/qwt_color_map.h \
    lib/qwt-6.1/src/qwt_scale_map.h \
    lib/qwt-6.1/src/qwt_interval.h \
    lib/qwt-6.1/src/qwt_point_polar.h \
    lib/qwt-6.1/src/qwt_transform.h \
    lib/qwt-6.1/src/qwt_abstract_legend.h \
    lib/qwt-6.1/src/qwt_abstract_scale.h \
    lib/qwt-6.1/src/qwt_abstract_scale_draw.h \
    lib/qwt-6.1/src/qwt_abstract_slider.h \
    lib/qwt-6.1/src/qwt_analog_clock.h \
    lib/qwt-6.1/src/qwt_arrow_button.h \
    lib/qwt-6.1/src/qwt_column_symbol.h \
    lib/qwt-6.1/src/qwt_compass.h \
    lib/qwt-6.1/src/qwt_compass_rose.h \
    lib/qwt-6.1/src/qwt_compat.h \
    lib/qwt-6.1/src/qwt_counter.h \
    lib/qwt-6.1/src/qwt_curve_fitter.h \
    lib/qwt-6.1/src/qwt_date.h \
    lib/qwt-6.1/src/qwt_date_scale_draw.h \
    lib/qwt-6.1/src/qwt_date_scale_engine.h \
    lib/qwt-6.1/src/qwt_dial.h \
    lib/qwt-6.1/src/qwt_dial_needle.h \
    lib/qwt-6.1/src/qwt_dyngrid_layout.h \
    lib/qwt-6.1/src/qwt_event_pattern.h \
    lib/qwt-6.1/src/qwt_graphic.h \
    lib/qwt-6.1/src/qwt_interval_symbol.h \
    lib/qwt-6.1/src/qwt_knob.h \
    lib/qwt-6.1/src/qwt_legend.h \
    lib/qwt-6.1/src/qwt_legend_data.h \
    lib/qwt-6.1/src/qwt_legend_label.h \
    lib/qwt-6.1/src/qwt_magnifier.h \
    lib/qwt-6.1/src/qwt_matrix_raster_data.h \
    lib/qwt-6.1/src/qwt_null_paintdevice.h \
    lib/qwt-6.1/src/qwt_painter_command.h \
    lib/qwt-6.1/src/qwt_panner.h \
    lib/qwt-6.1/src/qwt_picker.h \
    lib/qwt-6.1/src/qwt_picker_machine.h \
    lib/qwt-6.1/src/qwt_pixel_matrix.h \
    lib/qwt-6.1/src/qwt_plot.h \
    lib/qwt-6.1/src/qwt_plot_abstract_barchart.h \
    lib/qwt-6.1/src/qwt_plot_barchart.h \
    lib/qwt-6.1/src/qwt_plot_canvas.h \
    lib/qwt-6.1/src/qwt_plot_curve.h \
    lib/qwt-6.1/src/qwt_plot_dict.h \
    lib/qwt-6.1/src/qwt_plot_directpainter.h \
    lib/qwt-6.1/src/qwt_plot_glcanvas.h \
    lib/qwt-6.1/src/qwt_plot_grid.h \
    lib/qwt-6.1/src/qwt_plot_histogram.h \
    lib/qwt-6.1/src/qwt_plot_intervalcurve.h \
    lib/qwt-6.1/src/qwt_plot_item.h \
    lib/qwt-6.1/src/qwt_plot_layout.h \
    lib/qwt-6.1/src/qwt_plot_legenditem.h \
    lib/qwt-6.1/src/qwt_plot_magnifier.h \
    lib/qwt-6.1/src/qwt_plot_marker.h \
    lib/qwt-6.1/src/qwt_plot_multi_barchart.h \
    lib/qwt-6.1/src/qwt_plot_panner.h \
    lib/qwt-6.1/src/qwt_plot_picker.h \
    lib/qwt-6.1/src/qwt_plot_rasteritem.h \
    lib/qwt-6.1/src/qwt_plot_renderer.h \
    lib/qwt-6.1/src/qwt_plot_rescaler.h \
    lib/qwt-6.1/src/qwt_plot_scaleitem.h \
    lib/qwt-6.1/src/qwt_plot_seriesitem.h \
    lib/qwt-6.1/src/qwt_plot_shapeitem.h \
    lib/qwt-6.1/src/qwt_plot_spectrocurve.h \
    lib/qwt-6.1/src/qwt_plot_spectrogram.h \
    lib/qwt-6.1/src/qwt_plot_svgitem.h \
    lib/qwt-6.1/src/qwt_plot_textlabel.h \
    lib/qwt-6.1/src/qwt_plot_tradingcurve.h \
    lib/qwt-6.1/src/qwt_plot_zoneitem.h \
    lib/qwt-6.1/src/qwt_plot_zoomer.h \
    lib/qwt-6.1/src/qwt_point_3d.h \
    lib/qwt-6.1/src/qwt_point_data.h \
    lib/qwt-6.1/src/qwt_point_mapper.h \
    lib/qwt-6.1/src/qwt_raster_data.h \
    lib/qwt-6.1/src/qwt_round_scale_draw.h \
    lib/qwt-6.1/src/qwt_samples.h \
    lib/qwt-6.1/src/qwt_sampling_thread.h \
    lib/qwt-6.1/src/qwt_scale_div.h \
    lib/qwt-6.1/src/qwt_scale_draw.h \
    lib/qwt-6.1/src/qwt_scale_engine.h \
    lib/qwt-6.1/src/qwt_scale_widget.h \
    lib/qwt-6.1/src/qwt_series_data.h \
    lib/qwt-6.1/src/qwt_series_store.h \
    lib/qwt-6.1/src/qwt_slider.h \
    lib/qwt-6.1/src/qwt_spline.h \
    lib/qwt-6.1/src/qwt_symbol.h \
    lib/qwt-6.1/src/qwt_system_clock.h \
    lib/qwt-6.1/src/qwt_thermo.h \
    lib/qwt-6.1/src/qwt_wheel.h \
    lib/qwt-6.1/src/qwt_widget_overlay.h

SOURCES += \
    lib/qwt-6.1/src/qwt_text_engine.cpp \
    lib/qwt-6.1/src/qwt_text_label.cpp \
    lib/qwt-6.1/src/qwt_text.cpp \
    lib/qwt-6.1/src/qwt_painter.cpp \
    lib/qwt-6.1/src/qwt_math.cpp \
    lib/qwt-6.1/src/qwt_clipper.cpp \
    lib/qwt-6.1/src/qwt_color_map.cpp \
    lib/qwt-6.1/src/qwt_scale_map.cpp \
    lib/qwt-6.1/src/qwt_point_polar.cpp \
    lib/qwt-6.1/src/qwt_interval.cpp \
    lib/qwt-6.1/src/qwt_transform.cpp \
    lib/qwt-6.1/src/qwt_abstract_legend.cpp \
    lib/qwt-6.1/src/qwt_abstract_scale.cpp \
    lib/qwt-6.1/src/qwt_abstract_scale_draw.cpp \
    lib/qwt-6.1/src/qwt_abstract_slider.cpp \
    lib/qwt-6.1/src/qwt_analog_clock.cpp \
    lib/qwt-6.1/src/qwt_arrow_button.cpp \
    lib/qwt-6.1/src/qwt_column_symbol.cpp \
    lib/qwt-6.1/src/qwt_compass.cpp \
    lib/qwt-6.1/src/qwt_compass_rose.cpp \
    lib/qwt-6.1/src/qwt_counter.cpp \
    lib/qwt-6.1/src/qwt_curve_fitter.cpp \
    lib/qwt-6.1/src/qwt_date.cpp \
    lib/qwt-6.1/src/qwt_date_scale_draw.cpp \
    lib/qwt-6.1/src/qwt_date_scale_engine.cpp \
    lib/qwt-6.1/src/qwt_dial.cpp \
    lib/qwt-6.1/src/qwt_dial_needle.cpp \
    lib/qwt-6.1/src/qwt_dyngrid_layout.cpp \
    lib/qwt-6.1/src/qwt_event_pattern.cpp \
    lib/qwt-6.1/src/qwt_graphic.cpp \
    lib/qwt-6.1/src/qwt_interval_symbol.cpp \
    lib/qwt-6.1/src/qwt_knob.cpp \
    lib/qwt-6.1/src/qwt_legend.cpp \
    lib/qwt-6.1/src/qwt_legend_data.cpp \
    lib/qwt-6.1/src/qwt_legend_label.cpp \
    lib/qwt-6.1/src/qwt_magnifier.cpp \
    lib/qwt-6.1/src/qwt_matrix_raster_data.cpp \
    lib/qwt-6.1/src/qwt_null_paintdevice.cpp \
    lib/qwt-6.1/src/qwt_painter_command.cpp \
    lib/qwt-6.1/src/qwt_panner.cpp \
    lib/qwt-6.1/src/qwt_picker.cpp \
    lib/qwt-6.1/src/qwt_picker_machine.cpp \
    lib/qwt-6.1/src/qwt_pixel_matrix.cpp \
    lib/qwt-6.1/src/qwt_plot.cpp \
    lib/qwt-6.1/src/qwt_plot_abstract_barchart.cpp \
    lib/qwt-6.1/src/qwt_plot_axis.cpp \
    lib/qwt-6.1/src/qwt_plot_barchart.cpp \
    lib/qwt-6.1/src/qwt_plot_canvas.cpp \
    lib/qwt-6.1/src/qwt_plot_curve.cpp \
    lib/qwt-6.1/src/qwt_plot_dict.cpp \
    lib/qwt-6.1/src/qwt_plot_directpainter.cpp \
    lib/qwt-6.1/src/qwt_plot_glcanvas.cpp \
    lib/qwt-6.1/src/qwt_plot_grid.cpp \
    lib/qwt-6.1/src/qwt_plot_histogram.cpp \
    lib/qwt-6.1/src/qwt_plot_intervalcurve.cpp \
    lib/qwt-6.1/src/qwt_plot_item.cpp \
    lib/qwt-6.1/src/qwt_plot_layout.cpp \
    lib/qwt-6.1/src/qwt_plot_legenditem.cpp \
    lib/qwt-6.1/src/qwt_plot_magnifier.cpp \
    lib/qwt-6.1/src/qwt_plot_marker.cpp \
    lib/qwt-6.1/src/qwt_plot_multi_barchart.cpp \
    lib/qwt-6.1/src/qwt_plot_panner.cpp \
    lib/qwt-6.1/src/qwt_plot_picker.cpp \
    lib/qwt-6.1/src/qwt_plot_rasteritem.cpp \
    lib/qwt-6.1/src/qwt_plot_renderer.cpp \
    lib/qwt-6.1/src/qwt_plot_rescaler.cpp \
    lib/qwt-6.1/src/qwt_plot_scaleitem.cpp \
    lib/qwt-6.1/src/qwt_plot_seriesitem.cpp \
    lib/qwt-6.1/src/qwt_plot_shapeitem.cpp \
    lib/qwt-6.1/src/qwt_plot_spectrocurve.cpp \
    lib/qwt-6.1/src/qwt_plot_spectrogram.cpp \
    lib/qwt-6.1/src/qwt_plot_svgitem.cpp \
    lib/qwt-6.1/src/qwt_plot_textlabel.cpp \
    lib/qwt-6.1/src/qwt_plot_tradingcurve.cpp \
    lib/qwt-6.1/src/qwt_plot_xml.cpp \
    lib/qwt-6.1/src/qwt_plot_zoneitem.cpp \
    lib/qwt-6.1/src/qwt_plot_zoomer.cpp \
    lib/qwt-6.1/src/qwt_point_3d.cpp \
    lib/qwt-6.1/src/qwt_point_data.cpp \
    lib/qwt-6.1/src/qwt_point_mapper.cpp \
    lib/qwt-6.1/src/qwt_raster_data.cpp \
    lib/qwt-6.1/src/qwt_round_scale_draw.cpp \
    lib/qwt-6.1/src/qwt_sampling_thread.cpp \
    lib/qwt-6.1/src/qwt_scale_div.cpp \
    lib/qwt-6.1/src/qwt_scale_draw.cpp \
    lib/qwt-6.1/src/qwt_scale_engine.cpp \
    lib/qwt-6.1/src/qwt_scale_widget.cpp \
    lib/qwt-6.1/src/qwt_series_data.cpp \
    lib/qwt-6.1/src/qwt_slider.cpp \
    lib/qwt-6.1/src/qwt_spline.cpp \
    lib/qwt-6.1/src/qwt_symbol.cpp \
    lib/qwt-6.1/src/qwt_system_clock.cpp \
    lib/qwt-6.1/src/qwt_thermo.cpp \
    lib/qwt-6.1/src/qwt_wheel.cpp \
    lib/qwt-6.1/src/qwt_widget_overlay.cpp
