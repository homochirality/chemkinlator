/*
 * Copyright 2017-2019 Universidad Nacional de Colombia
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// NOLINT(build/header_guard)

#ifndef LIB_CPP_SIMULATIONDETAILS_H_
#define LIB_CPP_SIMULATIONDETAILS_H_

/**
 * @brief Holds additional information to the info found in ReactionDetails, this info is
 *        necessary for the fortran subroutine to work but is independent of
 *        ReactionDetails
 *
 * @tparam T Has been set to `double` and QLineEdit. The purpose to set it to `double` is
 *           to pass save the values to pass to the fortran subroutine, while the purpose
 *           of a SimulationDetails object when T is set to QLineEdit is to take the
 *           parameters values from the user.
 */
template <typename T>
class SimulationDetails {
  T Tmax;  //!< Final time for the simulation
  T Delta; //!< Size for each step in the simulation, e.g., if Delta = 0.001, T_0 = 0, and Tmax = 10, then the simulation will take 10001 steps to finish
  T ATOL;  //!< Look at dlsode documentation for info about this parameter
  T RTOL;  //!< Look at dlsode documentation for info about this parameter

public:
  SimulationDetails(T Tmax, T Delta, T ATOL, T RTOL)
      : Tmax(Tmax), Delta(Delta), ATOL(ATOL), RTOL(RTOL) {}
  SimulationDetails(const SimulationDetails&) = default;
  SimulationDetails(SimulationDetails&&) = default;
  SimulationDetails& operator=(const SimulationDetails&) = default;
  SimulationDetails& operator=(SimulationDetails&&) = default;
  ~SimulationDetails() = default;

  T getTmax()  const { return Tmax;  }
  T getDelta() const { return Delta; }
  T getATOL()  const { return ATOL;  }
  T getRTOL()  const { return RTOL;  }

  void setTmax(T Tmax) { this->Tmax = Tmax; }
  void setDelta(T Delta) { this->Delta = Delta; }
  void setATOL(T ATOL) { this->ATOL = ATOL; }
  void setRTOL(T RTOL) { this->RTOL = RTOL; }
};

#endif  // LIB_CPP_SIMULATIONDETAILS_H_
