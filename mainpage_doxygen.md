# ChemKinLator - Code documentation #

Welcome to the code documentation for chemkinlator. This documentation is meant for
developers. If you want to know how to compile and run chemkinlator, please go see the
download page: <https://gitlab.com/homochirality/chemkinlator>

If you want to modify/contribute on any part to the code, then this page is for you.

## General documentation ##

Chemkinlator's code has been documented in detail. If you want to start fiddling with the
code, we suggest you look at the following parts in order: Tabs (under `src/tabs/`),
Simulators (under `src/simulators/`), and the code behind the simulations (under
`lib/fortran/`, written, as you may have guess, in Fortran).

## How to add a new tab to chemkinlator ##

Creating a tab from zero requires three steps:

1. Define a new subclass of MainWindowTab and implement all its methods.
2. Connect the tab to `factorytab.cpp`.
3. Add the new tab to the compilation configuration files.

### Example ###

In this example we are going to create a simple tab that prints "Hello World" in console
and shows the user a small "Hello World" message. Note: You can find this example ready in
the git branch `new_tab_example`.

1. We create a new header file `src/tabs/exampletab.h`, and in it we write:

        #ifndef EXAMPLE_TAB_H
        #define EXAMPLE_TAB_H

        #include "tabs/mainwindowtab.h"

        class ExampleTab : public MainWindowTab {
          Q_OBJECT

        public:
          explicit ExampleTab(std::unique_ptr<MainWindowState> mwstate, QWidget *parent = Q_NULLPTR);

          ~ExampleTab() = default;

          void execute(const ReactionDetails &, const SimulationDetails<double> &);
          bool isStillRunning();
          void cleanTab();
          void reapplyBoxesState();
          QJsonObject toJsonObject();

        public slots:
          void replot();
        };

        #endif  // EXAMPLE_TAB_H

    Now, we create a new cpp file `src/tabs/exampletab.cpp`:

        #include <QDebug>
        #include <QMessageBox>
        #include "tabs/exampletab.h"

        ExampleTab::ExampleTab(std::unique_ptr<MainWindowState> mwstate, QWidget *parent)
          : MainWindowTab(std::move(mwstate), parent)
        {
          qDebug() << "ExampleTab created!";
        }

        void ExampleTab::execute(const ReactionDetails &, const SimulationDetails<double> &) {
          qDebug() << "Hello World from an ExampleTab";
          QMessageBox::information(this, "Hello World", "`Run simulation` button has been pressed");
        }

        bool ExampleTab::isStillRunning() { return false; }
        void ExampleTab::cleanTab() {}
        void ExampleTab::reapplyBoxesState() {}
        void ExampleTab::replot() {}

        QJsonObject ExampleTab::toJsonObject() {
          return QJsonObject { {"type", "example"} };
        }

2. We must connect the new tab to the main program. This is done in the `FactoryTab`. All
    we need to do is to add the following lines to the file `src/factorytab.cpp`:

    Include the new tab:

        #include "tabs/exampletab.h"

    Extend `getTabPtr` to return the new tab when asked:

        } else if (tabtype == "example") {
          toret = make_shared<ExampleTab>(std::move(mwstate), widget);

    Indicate in `tabTypes` what is the type and default name for your tab:

        {"example", "Example"},


3. Add the newly created class to `src/CMakeLists.txt` (or `chemkinlator.pro`, depending
    on which tool are you using to compile).

    In our example, we add `tabs/exampletab.cpp` to `src/CMakeLists.txt`:

        set(SOURCES
            main.cpp
            mainwindow.cpp
            mainwindowstate.cpp
            factorytab.cpp
            reactiondetails.cpp
            saverloader.cpp
            simulators/simulator.cpp
            simulators/time_series.cpp
            simulators/flow_temperature.cpp
            tabs/timeseriestab.cpp
            tabs/bifurcationtab.cpp
            tabs/flowtemperaturetab.cpp
            tabs/exampletab.cpp         # <<< Adding this line
            ui/dialogmatrices.cpp
            ui/newreactionswizard.cpp
            ${QWTMML_FILES}
        )

Now, just recompile and the new tab should be accessible from the tab menu.

And that is all! We have created a very simple tab. You are encouraged to look at all the
tabs inside `src/tabs/` for details on how each of them work. Spoiler: All current tabs
use one of the simulators found in `src/simulators/` which run a procedure in Fortran (all
Fortran procedures can be found in `lib/fortran/`).
